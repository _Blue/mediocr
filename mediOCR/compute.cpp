#include "stdafx.h"
#include "compute.h"
#include "textextraction.h"
#include "stdafx.h"
#include "spellcheck.h"
#include <locale.h>
typedef struct
{
	Extractfromimgparams *params;
	Extractfromimgresult *res;
}extractfromimgthreadparams;

static bool apply_dictionnary(char *data, const char *path);

DWORD WINAPI extractfromimg_worker(void *data) //Cannot be inlined
{
	setlocale(LC_NUMERIC, "C");//Locale seems to create problems with string to float convertions
	Extractfromimgparams *params = ((extractfromimgthreadparams*)data)->params;
	Extractfromimgresult *ret = ((extractfromimgthreadparams*)data)->res;
	free(data);
	Image *img = load_image(params->image);
	if (img == NULL)
	{
		ret->error = true;
	}
	else
	{
		set_logroutine(params->textout);
		set_maxthreads(params->maxthreads);
		if (params->rlsafixeradius)
		{
			forcerlsa_radius(params->rlsafixeradius);
		}
		if (params->rlsaprimaryradius)
		{
			setrlsa_radius(params->rlsaprimaryradius);
		}
		else
		{
			setrlsa_radius(RLSA_C);
		}
		USHORT lettercount;
		Letter **letters = detect_anddraw(img, params->colors, params->binarization, params->threshold, params->noise, params->rotation, &lettercount);
		Imagereadycallbackdata *ircb = (Imagereadycallbackdata*)malloc(sizeof(Imagereadycallbackdata));
		ircb->hwindow = params->hwindow;
		ircb->image = to_gdi(img);
		params->imageready((void*)ircb);
		free_image(img);

		ret->data = (char*)malloc(lettercount + 1);
		USHORT i;
		for (i = 0; i < lettercount;i++)
		{
			ret->data[i] = letters[i]->value;
		}
		ret->data[i] = '\0';
		if (params->dictionary != NULL)
		{
			char *final = apply_dictionary(ret->data, params->dictionary, letters);
			if (final == NULL)
			{
				MessageBox(NULL, "Failed to use dictionnary","mediOCR", MB_OK | MB_ICONERROR);
			}
			else
			{
				free(ret->data);
				ret->data = final;
			}
		}
		for (i = 0; i < lettercount; i++)
		{
			free_letter(letters[i]);
		}
		free(letters);
		set_logroutine(NULL);
		ret->error = false;
	}
	ret->hwindow = params->hwindow;
	params->cb(ret);
	if (params->dictionary != NULL)
	{
		free((void*)params->dictionary);
	}
	free(params);
	return 0;
}

OCRAPI void extractfromimg(Extractfromimgparams *params)
{
	Extractfromimgresult *ret = (Extractfromimgresult*)malloc(sizeof(Extractfromimgresult));
	extractfromimgthreadparams *threadp = (extractfromimgthreadparams*)malloc(sizeof(extractfromimgthreadparams));
	threadp->res = ret;
	threadp->params = params;
	HANDLE thread = CreateThread(NULL, NULL, extractfromimg_worker, threadp, NULL, NULL);
	if (thread == NULL)
	{
		critical("CreateThread failed %i", GetLastError());
		return;
	}
}
