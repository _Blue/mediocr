#ifndef COMPUTE_H

#include "debugapi.h"
typedef void (*Callback) (void *);
typedef struct
{
    bool colors; //Enables color recognition
    bool noise;
    bool rotation;
    int binarization;//Sets binarization
    int threshold;//Sets threshold
    USHORT rlsaprimaryradius;//First rlsa radius(0 for auto)
    USHORT rlsafixeradius;//Secondary rlsa radius (0 for auto)
    USHORT maxthreads;//Sets max thread count (0 for auto)
    Callback cb;//Fonction called when compute is done
	Callback imageready;//Called when the drawn image is ready (the image with the drawn rectangles)
	Gdiplus::Bitmap *image;
    const char*dictionary;
    Textlog textout;//Function  called when log requested
	HWND hwindow;//Handle to the main window
}Extractfromimgparams;

typedef struct
{
	char *data;//Actual data
    bool error;//True if an error occured
	HWND hwindow;//Handle to the main window
}Extractfromimgresult;

typedef struct
{
    int operation;
    const char *path;
    const char *result;
} Operationparams;

typedef struct
{
	HWND hwindow;
	Gdiplus::Bitmap *image;
}Imagereadycallbackdata;

void extractfromimg(_inout_ Extractfromimgparams *params);
/* Starts image analysis in a new thread, params will be deleted within the routine. When finished, the routine will
call params->cb with  an Extractfromimresult* as parameter*/

#endif
