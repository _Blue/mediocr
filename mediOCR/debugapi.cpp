#include "stdafx.h"
#include "debugapi.h"
#include <stdarg.h>

static void openlogfile();
static FILE *logfile = NULL;
static unsigned short imaxthreadcount=DEFAULT_MAXTHREADS;
static Textlog writefun=NULL;
#ifdef BUILD_RELEASE
bool bverbose = false;
#endif
void critical(const char *message, ...)
{
    char buf[DBG_MAX_CRITICAL_MESSAGE];
    va_list va;
    va_start(va, message);
    vsnprintf(buf, DBG_MAX_CRITICAL_MESSAGE - 1, message, va);
    va_end(va);
	buf[DBG_MAX_CRITICAL_MESSAGE - 1] = '\0';
	MessageBox(NULL, buf, "Critical failure", MB_ICONERROR | MB_OK);
    exit(0);
}

static OCRAPI void openlogfile() //Fonction doesn't need to be exported
{
    logfile = fopen(LOG_FILE, "w+");
    if(logfile == NULL)
    {
        printf("Warning : Failed to open logfile : \"%s\"", LOG_FILE);
    }
}

int ocrprintf(const char *format, ...)
{
    int ret;
    va_list va;
    va_start(va, format);
    ret = vprintf(format, va);
    if(logfile == NULL)
    {
        openlogfile();
        if(logfile == NULL)
            return ret;
    }
    va_end(va);
    va_start(va, format);
    vfprintf(logfile, format, va);
    va_end(va);
    return ret;
}

int verbose(const char *format, ...)
{
#ifdef BUILD_RELEASE
    if(!bverbose)
        return 0;
#endif
    int ret;
    va_list va;
    va_start(va, format);
    ret = vprintf(format, va);
    if(logfile == NULL)
    {
        openlogfile();
        if(logfile == NULL)
            return ret;
    }
    va_end(va);
    va_start(va, format);
    vfprintf(logfile, format, va);
    va_end(va);
    if(writefun!=NULL)
    {
        char buf[MAX_VERBOSEMSG];
        va_start(va, format);
        vsnprintf(buf, MAX_VERBOSEMSG,format, va);
        writefun(buf);
        va_end(va);
    }
    return ret;
}


OCRAPI int ocrputs(const char *data)
{
    return ocrprintf(data);
}

OCRAPI int toint(float f)
{
    if(f - (int)f > 0.5)
        return (int)(f) + 1;
    else
        return (int)f;
}

OCRAPI LARGE_INTEGER init_time()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return li;
}

OCRAPI unsigned long timediff(LARGE_INTEGER t)
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return (li.LowPart - t.LowPart)/1000;
}

OCRAPI void setverbose(bool b)
{
#ifdef BUILD_RELEASE
    bverbose = b;
#endif
    b = b; //so ugly, but because of Werror, b has to be used
}

OCRAPI float MAXF(float f1, float f2)
{
    if(f1 > f2)
        return f1;
    return f2;
}

OCRAPI float MINF(float f1, float f2)
{
    if(f1 < f2)
        return f1;
    return f2;
}

OCRAPI int MAX(int f1, int f2)
{
    if(f1 > f2)
        return f1;
    return f2;
}

OCRAPI int MIN(int f1, int f2)
{
    if(f1 < f2)
        return f1;
    return f2;
}

OCRAPI float ABS(float f)
{
    if(f >= 0)
        return f;
    return -f;
}

OCRAPI unsigned short get_maxthreads()
{
    return imaxthreadcount;
}

OCRAPI void set_maxthreads(unsigned short i)
{
    imaxthreadcount=i;
}
OCRAPI bool rect_equals(Rect a, Rect b)
{
    return a.x==b.x&&a.y==b.y&&a.w==b.w&&a.h==b.h;
}

OCRAPI void set_logroutine(Textlog log)
{
    writefun=log;
}
