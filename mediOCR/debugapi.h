/// File : debugapi.h
/// Purpose : Contains debug fonctions, overrides printfs and puts
#ifndef DEBUGAPI_H
#define DEBUGAPI_H
#ifdef BUILD_DEBUG
#define puts ocrputs
#define printf ocrprintf
#endif
#include "stdafx.h"
#include <time.h>

typedef struct timeval timeval;
typedef void (*Textlog)(const char *);

void critical(_in_ const char *message, ...);
/* Shows failure message and exits
Safe if message is valid*/

int ocrprintf(_in_ const char*, ...);
/* writes in stdout and in log file, safe if parameters are valid*/

int verbose(_in_ const char*, ...);
/* writes only if verbose is enabled or building in debug taget*/

int ocrputs(_in_ const char*);
/* writes in stdout and in log file, safe if parameters are valid*/

LARGE_INTEGER init_time();
/* returns current timeval, wich can be used to measure precise time*/

unsigned long timediff(LARGE_INTEGER t);
/* returns difference between t and current time in ms*/

void setverbose(bool);
/* enables or disables verbose status*/

unsigned short get_maxthreads();
/* returns maximim thread number (can be modified by -j[number] option*/

void set_maxthreads(unsigned short );
/* set maximum thread number*/

void set_logroutine(Textlog );
//Set current verbose routine
#endif
