#include "stdafx.h"
#include "filters.h"
#include <assert.h>
#include <math.h>

Bin_Image *binarize(Image *img, int bin_type, int threshold,bool noise,bool rotation)
{
    UNUSED(rotation);
    Gray_Image *gimg = apply_grayscale(img);
    if(noise)
    {
        Gray_Image *old=gimg;
        gimg=apply_median_filter(old);
        free_grayimage(old);
    }
    Bin_Image *bimg;
    if (bin_type == OTSU1)
        bimg = apply_otsu(gimg);
    else if (bin_type == OTSU2)
        bimg = apply_otsu2(gimg);
    else if (bin_type == SAUVOLA)
        bimg = apply_sauvola(gimg);
    else
        bimg = apply_threshold(gimg, threshold);
    free_grayimage(gimg);
    return bimg;
}

Bin_Image *apply_threshold(Gray_Image *img, int threshold)
{
    Bin_Image *s = create_bin_image(img->w, img->h);

    for(USHORT x = 0; x < img->w; x++)
    {
        for(USHORT y = 0; y < img->h; y++)
        {
            BYTE px = getpixelgray(img, x, y);
            setpixelbin(s, x, y, px <= threshold);
        }
    }
    return s;
}

Bin_Image *apply_otsu(Gray_Image *img)
{
    int histogram[256] = {0};
    get_histogram(img, histogram);

    float sum = 0;
    for (int i = 0; i < 255; i++)
        sum += i * histogram[i];

    float sumB = 0, wB = 0, wF = 0, varmax = 0, threshold = 0;
    int pxnbr = img->w * img->h;

    for (int i = 0; i < 255; i++)
    {
        wB += (int)histogram[i];
        if (wB == 0)
            continue;

        wF = pxnbr - wB;
        if (wF == 0)
            break;

        sumB += (float)(i * histogram[i]);

        float mB = sumB / wB;
        float mF = (sum - sumB) / wF;

        float mBF = mB - mF;
        float variance = wB * wF * mBF * mBF;

        if (variance > varmax)
        {
            varmax = variance;
            threshold = (float)i;
        }
    }

    return apply_threshold(img, (int)threshold);
}

Bin_Image *apply_otsu2(Gray_Image *img)
{
    int histogram[256] = {0};
    get_histogram(img, histogram);

    double w = 0.0;
    double u = 0.0;
    double uT = 0.0;

    int k = 256;
    int threshold = 0;

    double histNormalized[256];

    long double work1 = 0.0, work2 = 0.0;
    double work3 = 0.0;

    for (int i = 1; i <= k; ++i)
        histNormalized[i - 1] = histogram[i - 1] / (double)(img->w * img->h);

    for (int i = 1; i <= k; ++i)
        uT += i * histNormalized[i - 1];

    for (int i = 1; i <= k; ++i)
    {
        w += histNormalized[i - 1];
        u += i * histNormalized[i - 1];
        work1 = uT * w - u;
        work2 = (work1 * work1) / (w * (1.0f - w));
        if (work2 > work3)
            work3 = work2;
    }
    threshold = toint((float)round(sqrt(work3)));
    return apply_threshold(img, threshold);


}

/**
 * \brief Calculates the average value of the histogram
 *
 * \param hist The histogram of a grayscale image
 */
int mean(int hist[256])
{
    int m = 0;
    for (USHORT i = 0; i < 256; ++i)
        m += i * hist[i];
    return m;
}

Bin_Image *apply_sauvola(Gray_Image *img)
{
    int histogram[256] = {0};
    get_histogram(img, histogram);

    long size = img->w * img->h;
	float m = (float)(mean(histogram) / size);
    float pixel_sum = 0.0f;
	for (long x = 0; x < img->w; ++x)
	{
		for (long y = 0; y < img->h; ++y)
		{
			float v = getpixelgray(img,(USHORT) x, (USHORT)y) - m;
			pixel_sum += v * v;
		}
	}
    float k = (float)0.2;
    float R = 128;
    float s = 1 / sqrt(size * pixel_sum);
    return apply_threshold(img, (int)(m * (1 - k * (1 - s/R))));
}



Gray_Image *apply_grayscale(Image* img)
{
    Gray_Image *s = create_gray_image(img->w, img->h);

    for(USHORT x = 0; x < img->w; x++)
    {
        for(USHORT y = 0; y < img->h; y++)
        {
            Pixel px = getpixel(img, x, y);
            BYTE gray = (BYTE)toint((float)((px.red + px.green + px.blue) / 3));
            setpixelgray(s, x, y, gray);
        }
    }

    return s;
}

Image *apply_gaussblur(Image* img, USHORT length)
{

    Image *s = create_image(img->w, img->h);
    USHORT x;
    USHORT y;
    short mx;
    short my;
    float r, g, b, factor;
    Pixel current;
    Pixel sub;
    for(x = 0; x < img->w; x++)
    {
        for(y = 0; y < img->h; y++)
        {
            r = 0;
            g = 0;
            b = 0;
            current = getpixel(img, x, y);
            for(mx = x - length; mx < x + length; mx++)
            {
                for(my = y - length; my < y + length; my++)
                {
                    if(mx == x && my == y)
                    {
                        r += current.red;
                        g += current.green;
                        b += current.blue;
                    }
                    else
                    {
                        factor =(float)( 1 / ((float)(MAX((int)ABS((float)(mx - x)),(int)ABS((float)((float)(my - y))) + 1) * 4 - 4)));
                        sub = getpixel_s(img, mx, my, current);
                        r += sub.red * factor;
                        g += sub.green * factor;
                        b += sub.blue * factor;
                    }
                }
            }
            setpixelrgb(s, x, y,(BYTE)(r / (2 * length)),(BYTE)(g / (2 * length)),(BYTE) b / (2 * length));
        }
    }
    return s;
}

static inline void insertion_sort(USHORT *a, int n)
{
    for (int i = 1; i < n; ++i)
    {
        int x = a[i];
        int j = i;
        for (; j > 0 && a[j - 1] > x; --j)
        {
            a[j] = a[j - 1];
        }
        a[j] = x;
    }
}

Gray_Image *apply_median_filter(Gray_Image *img)
{
    Gray_Image *newimg = create_gray_image(img->w, img->h);
    // window size
    int wsize = 3;
    USHORT *window = (USHORT*)malloc(sizeof(USHORT) * wsize * wsize);
    int edgex = 1;
    int edgey = 1;
    for (int x = 0; x < img->w; ++x)
    {
        for (int y = 0; y < img->h; ++y)
        {
            int i = 0;
            for (int fx = -edgex; fx <= edgex; ++fx)
            {
                for(int fy = -edgey; fy <= edgey; ++fy)
                {
                    int v = x + edgex;
                    int w = y + edgey;
                    if (v < 0 || v >= img->h || w < 0 || w >= img->h)
                    {
                        window[i] = 255;
                    }
                    else
                    {
                        window[i] = getpixelgray(img, v, w);
                    }
                    i++;
                }
            }
            // Use this algorithm because divide and conquer-based algorithms
            // use a lot of recursion and can deteriorate the performances
            insertion_sort(window, wsize * wsize);
            setpixelgray(newimg, x, y, (BYTE)(window[(wsize * wsize) / 2]));
        }
    }
    free(window);
    return newimg;
}
