/// File : filters.h
///Purpose : Exports filtering fonctions
///like grayscale, binarization, blur, etc

#ifndef FILTERS_H
#define FILTERS_H

#include "stdafx.h"
#include "imgmanip.h"

/**
 * \brief Binarize the image using one of the 4 methods
 *
 * \param bin_type Used to determine which method to use (must be THRESHOLD,
 * OTSU1, OTSU2 or SAUVOLA
 * \param threshold Given to apply_threshold if bin_type is set to THRESHOLD
 */
Bin_Image *binarize(_in_ Image *img, int bin_type, int threshold,bool noise,bool rotation);

/**
 * \brief Apply a fixed threshold on the image
 */
Bin_Image *apply_threshold(_in_ Gray_Image*, int threshold);

/**
 * \brief Apply Otsu's method to binarize the image
 *
 * Algorithm found on: <http://zerocool.is-a-geek.net/java-image/binarization/>
 */
Bin_Image *apply_otsu(_in_ Gray_Image *img);

/**
 * \brief Apply Otsu's method to binarize the image
 *
 * Algorithm found on: <http://dandiggins.co.uk/arlib-9.html>
 */
Bin_Image *apply_otsu2(_in_ Gray_Image *img);

/**
 * \brief Apply Sauvola's method to binarize the image
 *
 * Formula found on: <http://www.math-info.univ-paris5.fr/~vincent/articles/DRR_nick_binarization_09.pdf>
 */
Bin_Image *apply_sauvola(_in_ Gray_Image *img);

/**
 * \brief Returns a grayscale version of the image
 */
Gray_Image *apply_grayscale(_in_ Image*);

/**
 * \brief Applies a gaussian blur to the image
 * \param length Convolution matrix's size
 */
Image *apply_gaussblur(_in_ Image*, USHORT length);

/*
 * Returns a copy of the image after the application of a median filter
 * The size of the matrix is 3
 */

Gray_Image *apply_median_filter(Gray_Image *img);

#endif
