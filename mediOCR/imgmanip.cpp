#include "stdafx.h"
#include "imgmanip.h"
#include <math.h>
#ifdef BUILD_DEBUG
#include "debugapi.h"
#endif
#include <assert.h>
#include "textextraction.h"
#include "filters.h"

///Internal fonctions
static int getxoffset(int w, int h, float cos, float sin, USHORT ox, USHORT oy);
static int getyoffset(int w, int h, float cos, float sin, USHORT ox, USHORT oy);
static Rect getusefulrect(_in_ Bin_Image* img);
static bool getpixelbin_raw(_in_ bool*, USHORT x, USHORT y, USHORT w);
static void adjust_block(_in_ Bin_Image *img, Rect *r);

///Pixel access fonctions
OCRAPI Pixel getpixel(Image *img, USHORT x, USHORT y)
{
#ifdef BUILD_DEBUG
	if (img->w <= x || img->h <= y)
	{
		critical("Position : (%i, %i)"
			" is out of bounds of image (%i, %i) in getpixel", x, y, img->w, img->h);
	}
#endif

	return img->data[y * img->w + x];
}

OCRAPI void setpixel(Image *img, USHORT x, USHORT y, Pixel px)
{
#ifdef BUILD_DEBUG
	if (img->w <= x || img->h <= y)
	{
		critical("Position : (%i, %i)"
			"is out of bounds of image (%i, %i) in getpixel", x,
			y, img->w, img->h);
	}
#endif

	img->data[y * img->w + x] = px;
}

OCRAPI void setpixel_s(Image *img, short x, short y, Pixel px)
{
	if (img->w <= x || x < 0 || y < 0 || img->h <= y)
	{
		printf("Warning : pixel : (%i, %i)"
			"is out of bounds (%i, %i)\n", x, y, img->w,
			img->h);
		return;
	}
	setpixel(img, x, y, px);
}

OCRAPI Pixel getpixel_s(Image *img, short x, short y, Pixel def)
{
	if (x >= img->w || y >= img->h || x < 0 || y < 0)
		return def;
	else
		return getpixel(img, x, y);
}

OCRAPI void setpixelrgb(Image *img, USHORT x, USHORT y, BYTE R, BYTE G,
	BYTE B)
{
	Pixel px;
	px.red = R;
	px.green = G;
	px.blue = B;

	setpixel(img, x, y, px);
}

OCRAPI void setpixelgray(Gray_Image *img, USHORT x, USHORT y, BYTE b)
{
	assert(img != NULL);
#ifdef BUILD_DEBUG
	if (x >= img->w || y >= img->h)
	{
		printf("Warning: position : (%i, %i)"
			"is out of bounds of image (%i, %i)\n", x,
			y, img->w, img->h);
		critical("Invalid position in setpixelgray");
	}
#endif

	img->data[x + y * img->w] = b;
}

OCRAPI BYTE getpixelgray(Gray_Image *img, USHORT x, USHORT y)
{
	assert(img != NULL);
#ifdef BUILD_DEBUG
	if (x >= img->w || y >= img->h)
	{
		printf("Warning: position : (%i, %i)"
			"is out of bounds of image (%i, %i)\n", x,
			y, img->w, img->h);
		critical("Invalid position in getpixelgray");
	}
#endif

	return img->data[x + y * img->w];
}

OCRAPI void get_histogram(Gray_Image *img, int h[256])
{
	for (USHORT x = 0; x < img->w; ++x)
	{
		for (USHORT y = 0; y < img->h; ++y)
		{
			h[getpixelgray(img, x, y)]++;
		}
	}
}

OCRAPI void setpixelbin(Bin_Image *img, USHORT x, USHORT y, bool b)
{
#ifdef BUILD_DEBUG
	if (x >= img->w || y >= img->h)
	{
		critical("Pixel (%u, %u) is out of bounds (%u, %u) in getpixelbin", x, y,
			img->w, img->h);
	}
#endif

	img->data[x + y * img->w] = b;
}

OCRAPI bool getpixelbin(Bin_Image *img, USHORT x, USHORT y)
{
#ifdef BUILD_DEBUG
	if (x >= img->w || y >= img->h)
	{
		critical("Pixel (%u, %u) is out of bounds (%u, %u) in getpixelbin", x, y,
			img->w, img->h);
	}
#endif

	return img->data[x + y * img->w];
}

static OCRAPI bool getpixelbin_raw(_in_ bool *data, USHORT x, USHORT y,
	USHORT w)
{
#ifdef BUILD_DEBUG
	if (x >= w)
	{
		printf("Warning: position : (%i, %i)"
			"is out of bounds of image (%i, ?)\n", x, y,
			w);
		critical("Invalid position in getpixelbin_raw");
	}
#endif
	return data[x + y * w];
}

OCRAPI Pixel getpixel_raw(Pixel *data, USHORT x, USHORT y, USHORT w)
{
	return data[x + y * w];
}

OCRAPI BYTE getpixel_asgray(Image *img, USHORT x, USHORT y)
{
#ifdef BUILD_DEBUG
	if (img->w <= x || img->h <= y)
	{
		printf("Warning : position : (%i, %i)"
			"is out of bounds of image (%i, %i)", x, y,
			img->w, img->h);
		critical("Invalid position in getpixel_asgray");
	}
#endif // BUILD_DEBUG
	Pixel px = getpixel(img, x, y);
	return (BYTE)(0.21 * px.red + 0.72 * px.green + 0.07 * px.blue);
}

///Data query fonctions
Image *bimage_tosurface(Bin_Image *img)
{
	Image *ret = create_image(img->w, img->h);
	USHORT x;
	USHORT y;
	Pixel empty = { 0, 0, 0 };
	Pixel full = { 255, 255, 255 };
	for (x = 0; x < img->w; x++)
	{
		for (y = 0; y < img->h; y++)
		{
			if (getpixelbin(img, x, y))
				setpixel(ret, x, y, empty);
			else
				setpixel(ret, x, y, full);
		}
	}
	return ret;
}

Image *gimage_to_image(Gray_Image *img)
{
	Image *ret = create_image(img->w, img->h);
	USHORT x;
	USHORT y;
	for (x = 0; x < img->w; x++)
	{
		for (y = 0; y < img->h; y++)
		{
			USHORT gr = getpixelgray(img, x, y);
			Pixel pxgr = { (BYTE)gr, (BYTE)gr, (BYTE)gr };
			setpixel(ret, x, y, pxgr);
		}
	}
	return ret;
}

OCRAPI Bin_Image *sub_surface(Bin_Image *img, USHORT x, USHORT y, USHORT w,
	USHORT h)
{
#ifdef BUILD_DEBUG
	if (x + w > img->w || y + h > img->h)
	{
		printf("Warning : Size of sub image"
			"(x : %u, y : %u, w : %u, h : %u) is out of bounds",
			x, y, w, h);
	}
#endif
	USHORT cx, cy;
	Bin_Image *ret = create_bin_image(w, h);
	for (cx = x; cx < x + w && cx <= img->w; cx++)
	{
		for (cy = y; cy < y + h && cy <= img->h; cy++)
		{
			setpixelbin(ret, cx - x, cy - y, getpixelbin(img, cx, cy));
		}
	}
	return ret;
}

OCRAPI bool islineblank(Bin_Image *img, USHORT line)
{
#ifdef BUILD_DEBUG
	if (line >= img->h)
	{
		critical("islineblank called with invalid line (%u) (max : %u)\n", line,
			img->h);
	}
#endif
	USHORT i;
	for (i = 0; i < img->w; i++)
	{
		if (getpixelbin(img, i, line))
			return false;
	}
	return true;
}

OCRAPI bool iscolumnblank(Bin_Image *img, USHORT c)
{
#ifdef BUILD_DEBUG
	if (c >= img->w)
	{
		critical("islineblank called with invalid column (%u) (max : %u)", c,
			img->w);
	}
#endif
	USHORT i;
	for (i = 0; i < img->h; i++)
	{
		if (getpixelbin(img, c, i))
			return false;
	}
	return true;
}

static OCRAPI Rect getusefulrect(Bin_Image *img)
{
	Rect rect;
	for (rect.x = 0; rect.x < img->w - 1; rect.x++) //left border
	{
		if (!iscolumnblank(img, rect.x + 1))
			break;
	}

	if (rect.x == img->w - 1)
	{
		printf("Image is totaly blank, nothing could be extracted\n");
		exit(0);
	}
	for (rect.w = img->w - 1; rect.w > 1; rect.w--) //right border
	{
		if (!iscolumnblank(img, rect.w - 1))
			break;
	}

	rect.w -= rect.x;
	for (rect.y = 0; rect.y < img->h - 1; rect.y++) //top border
	{
		if (!islineblank(img, rect.y + 1))
			break;
	}

	for (rect.h = img->h - 1; rect.h > 1; rect.h--) //bottom border
	{
		if (!islineblank(img, rect.h - 1))
			break;
	}
	rect.h -= rect.y;
	return rect;
}

///Memory cleaning fonctions
OCRAPI void free_bimage(Bin_Image *img)
{
	free(img->data);
	free(img);
}

OCRAPI void free_grayimage(Gray_Image *img)
{
	free(img->data);
	free(img);
}

OCRAPI void free_image(Image *img)
{
	free(img->data);
	free(img);
}

///Image creation fonctions

OCRAPI Gray_Image *create_gray_image(USHORT w, USHORT h)
{
	Gray_Image *ret = (Gray_Image*)malloc(sizeof(Gray_Image));
	ret->w = w;
	ret->h = h;
	ret->data = (BYTE*)malloc(sizeof(BYTE) * w * h);
	return ret;
}

OCRAPI Bin_Image *create_bin_image(USHORT w, USHORT h)
{
	Bin_Image *ret = (Bin_Image*)malloc(sizeof(Bin_Image));
	ret->w = w;
	ret->h = h;
	ret->data = (bool*)malloc(sizeof(bool) * w * h);
	return ret;
}

OCRAPI Image *create_image(USHORT w, USHORT h)
{
	Image *img = (Image*)malloc(sizeof(Image));
	img->w = w;
	img->h = h;
	img->data = (Pixel*)malloc(sizeof(Pixel) * w * h);
	return img;
}

///Image processing fonctions

Bin_Image *get_resizedsquare(Image *img, USHORT x, USHORT y, USHORT w, USHORT h, USHORT neww, USHORT newh)
{
#ifdef BUILD_DEBUG
	if (x + w > img->w || y + h > img->h)
	{
		critical("Rectangle (%u, %u, %u, %u) is out of bounds (%u, %u) in get_resizedsquare", x, y, w, h, img->w, img->h);
	}
#endif
	DWORD **data = (DWORD**)malloc(sizeof(DWORD)*neww);
	float **count = (float**)malloc(sizeof(float)*neww);
	int i;
	for (i = 0; i < neww; i++)
	{
		data[i] = (DWORD*)malloc(sizeof(DWORD)*newh);
		count[i] = (float*)malloc(sizeof(float)*newh);
		ZeroMemory(data[i], newh * sizeof(DWORD));
		ZeroMemory(count[i], newh * sizeof(float));
	}
	float fx;
	float fy;
	float leftfactor;
	float upfactor;
	USHORT cx;
	USHORT cy;
	BYTE p;
	for (cx = x; cx < x + w; cx++)
	{
		fx = (cx - x) * (neww / (float)w);
		leftfactor = 1 - (fx - (USHORT)fx);
		for (cy = y; cy < y + h; cy++)
		{
			fy = (cy - y) * (newh / (float)h);
			upfactor = 1 - (fy - (USHORT)fy);
			p = getpixel_asgray(img, cx, cy);
			data[(USHORT)fx][(USHORT)fy] += (DWORD)(p * leftfactor * upfactor);
			count[(USHORT)fx][(USHORT)fy] += leftfactor * upfactor;
			if (fx < neww - 1)
			{
				data[(USHORT)fx + 1][(USHORT)fy] += (DWORD)(p *(1 - leftfactor) * upfactor);
				count[(USHORT)fx + 1][(USHORT)fy] += ((1 - leftfactor) * upfactor);
			}
			if (fy < newh - 1)
			{
				data[(USHORT)fx][(USHORT)fy + 1] += (DWORD)(p *leftfactor * (1 - upfactor));
				count[(USHORT)fx][(USHORT)fy + 1] += (leftfactor *(1 - upfactor));
				if (fx < neww - 1)
				{
					data[(USHORT)fx + 1][(USHORT)fy + 1] += (DWORD)(p *(1 - leftfactor) * (1 - upfactor));
					count[(USHORT)fx + 1][(USHORT)fy + 1] += ((1 - leftfactor) * (1 - upfactor));
				}
			}
		}
	}
	Gray_Image *gray = create_gray_image(neww, newh);
	for (cy = 0; cy < newh; cy++)
	{
		for (cx = 0; cx < neww; cx++)
		{
			setpixelgray(gray, cx, cy, (BYTE)(data[cx][cy] / count[cx][cy]));
		}
	}
	Bin_Image *ret = apply_threshold(gray, 200); //Should use sauvola when available
	free_grayimage(gray);

	for (i = 0; i < neww; i++)
	{
		free(data[i]);
		free(count[i]);
	}

	free(data);
	free(count);

	return ret;
}

static OCRAPI int getxoffset(int w, int h, float c, float s, USHORT originx,
	USHORT originy)
{
	int buf = (int)(c * (-originx) - s * (-originy) + originx);
	int ret = buf;

	buf = (int)(c * (w - originx) - s * (-originy) + originx);
	if (buf < ret)
		ret = buf;

	buf = (int)(c * (w - originx) - s * (h - originy) + originx);
	if (buf < ret)
		ret = buf;

	buf = (int)(c * (-originx) - s * (h - originy) + originx);
	if (buf < ret)
		ret = buf;
	return -ret;
}

static OCRAPI int getyoffset(int w, int h, float c, float s, USHORT originx,
	USHORT originy)
{
	int buf = (int)(s * (-originx) + c * (-originy) + originy);
	int ret = buf;

	buf = (int)(s * (w - originx) + c * (-originy) + originy);
	if (buf < ret)
		ret = buf;

	buf = (int)(s * (w - originx) + c * (h - originy) + originy);
	if (buf < ret)
		ret = buf;

	buf = (int)(s * (-originx) + c * (h - originy) + originy);
	if (buf < ret)
		ret = buf;
	return -ret;
}

void rotated_histogram(Gray_Image *img, float angle, USHORT originx, USHORT originy, USHORT hist[256])
{
	memset(hist, 0, 256);
	float c = cos(angle);
	float s = sin(angle);
	int xoffset = getxoffset(img->w, img->h, c, s, originx, originy);
	int yoffset = getyoffset(img->w, img->h, c, s, originx, originy);
	int y = 0;
	for (int x = 0; x < img->w; ++x)
	{
		USHORT lx = toint(c * (x - originx) - s * (y - originy) + originx)
			+ xoffset;
		for (y = 0; y < img->h; ++y)
		{
			USHORT cx = toint(c * (x - originx) - s *(y - originy) + originx) + xoffset;
			USHORT cy = toint(s * (x - originx) + c * (y - originy) + originy) + yoffset;
			if (ABS((float)(cx - lx)) >= 1)
			{
				if (cx > lx)
				{
					hist[getpixelgray(img, cx - 1, cy)]++;
				}
				else
				{
					hist[getpixelgray(img, cx + 1, cy)]++;
				}
			}
			hist[getpixelgray(img, cx, cy)]++;
			lx = cx;
		}
	}
}

// Should only be used by `detect_skew`
static float variance(USHORT hist[], size_t n)
{
	int mean = 0;
	size_t i = 0;
	for (; i < n; ++i)
	{
		mean += hist[i];
	}
	mean /= i;

	float sum = 0;
	for (i = 0; i < n; ++i)
	{
		int x = hist[i] - mean;
		sum += x * x;
	}
	return sum / (n - 1);
}

float detect_skew(Gray_Image *img)
{
	USHORT *hist = (USHORT*)malloc(sizeof(USHORT) * 256);
	float bestangle = 0;
	float varmax = 0;
	for (int a = -25; a <= 25; ++a)
	{
		rotated_histogram(img, (float)a, 0, 0, hist);
		float var = variance(hist, 256);
		if (var > varmax)
		{
			bestangle = (float)a;
			varmax = var;
		}
	}
	return bestangle;
}

void rotate_image(Image *img, float angle, USHORT originx, USHORT originy)
{
	int x, y;
	float c, s;
	USHORT basex = img->w;
	USHORT basey = img->h;
	USHORT cx = 0;
	USHORT cy = 0;
	c = cos(angle);
	s = sin(angle);
	void *data = img->data;
	img->w = (USHORT)(ceil(basex * ABS(c) + basey * ABS(s)) + 1);
	img->h = (USHORT)(ceil(basex * ABS(s) + basey * ABS(c)) + 1);
	img->data = (Pixel*)malloc(sizeof(Pixel) * img->h * img->w);
	ZeroMemory(img->data, sizeof(Pixel)*img->w * img->h);
	int xoffset = getxoffset(basex, basey, c, s, originx, originy);
	int yoffset = getyoffset(basex, basey, c, s, originx, originy);
	USHORT lx;
	y = 0;
	for (x = 0; x < basex; x++)
	{
		lx = toint(c * (x - originx) - s * (y - originy) + originx)
			+ xoffset;
		for (y = 0; y < basey; y++)
		{
			cx = toint(c * (x - originx) - s *
				(y - originy) + originx) + xoffset;
			cy = toint(s * (x - originx) + c *
				(y - originy) + originy) + yoffset;
			if (ABS((float)(cx - lx)) >= 1)
			{
				if (cx > lx)
				{
					setpixel_s(img, cx - 1, cy, getpixel_raw((Pixel*)data, x, y, basex));
				}
				else
				{
					setpixel_s(img, cx + 1, cy, getpixel_raw((Pixel*)data, x, y, basex));
				}
			}
			setpixel_s(img, cx, cy, getpixel_raw((Pixel*)data, MIN(x, basex - 1), MIN(y, basey - 1), basex));
			lx = cx;
		}
	}
	free(data);
}

void remove_blank(Bin_Image *img, USHORT *xoffset, USHORT *yoffset)
{
	bool *bak = img->data;
	Rect rect = getusefulrect(img);
	USHORT x, y, w;
	w = img->w;
	img->w = rect.w;
	img->h = rect.h;
	img->data = (bool*)malloc(img->w * img->h * sizeof(bool));
	for (x = 0; x < rect.w; x++)
	{
		for (y = 0; y < rect.h; y++)
		{
			setpixelbin(img, x, y, getpixelbin_raw(bak, x + rect.x, y + rect.y, w));
		}
	}
	free(bak);
	if (xoffset != NULL)
		*xoffset = rect.x;
	if (yoffset != NULL)
		*yoffset = rect.y;
}

OCRAPI void drawrect(Image *img, USHORT x, USHORT y, USHORT w, USHORT h,
	Pixel p)
{
	USHORT c;
	for (c = x; c < x + w + 1 && c < img->w; c++)
	{
		setpixel(img, c, y, p);
	}
	for (c = x; c < x + w + 1 && c < img->w; c++)
	{
		setpixel(img, c, y + h, p);
	}
	for (c = y; c < y + h; c++)
	{
		setpixel(img, x, c, p);
	}

	for (c = y; c < y + h; c++)
	{
		setpixel(img, x + w, c, p);
	}
}

OCRAPI void drawfromrect(Image *i, Rect r, Pixel c)
{
	drawrect(i, r.x, r.y, r.w, r.h, c);
}

OCRAPI Rect mergerects(Rect r1, Rect r2)
{
	Rect r;
	r.x = MIN(r1.x, r2.x);
	r.y = MIN(r1.y, r2.y);
	r.w = MAX(r1.w + r1.x, r2.x + r2.w) - r.x;
	r.h = MAX(r1.y + r1.h, r2.y + r2.h) - r.y;
	return r;
}

OCRAPI void addletter(Letter *l1, Letter l2)
{
	Rect r = l1->rect;
	l1->rect = mergerects(l1->rect, l2.rect);
	int x;
	int y;
	Bin_Image *img = l1->data;
	l1->data = create_bin_image(l1->rect.w, l1->rect.h);
	ZeroMemory(l1->data->data, sizeof(bool)*l1->data->w * l1->data->h);
	for (x = l1->rect.x; x < l1->rect.x + l1->rect.w; x++)
	{
		for (y = l1->rect.y; y < l1->rect.y + l1->rect.h; y++)
		{
			if ((x >= r.x && y >= r.y && x < r.w + r.x && y < r.h + r.y
				&& getpixelbin(img, x - r.x, y - r.y)) || (x >= l2.rect.x && y >= l2.rect.y
				&& //That boolean
				x - l2.rect.x < l2.rect.w && y - l2.rect.y < l2.rect.h
				&& getpixelbin(l2.data, x - l2.rect.x, y - l2.rect.y)))
			{
				setpixelbin(l1->data, x - l1->rect.x, y - l1->rect.y, true);
			}
		}
	}
	free_bimage(img);
}

OCRAPI Point *createpoint(USHORT x, USHORT y)
{
	Point *ret = (Point*)malloc(sizeof(Point));
	ret->x = x;
	ret->y = y;
	return ret;
}

OCRAPI void prepareletter_fornn(Letter *l, USHORT w, USHORT h, Image *img)
{
#ifdef BUILD_DEBUG
	l->originaldata = l->data;
	if (w != h)
	{
		printf("Warning: w!=h in prepareletter_fornn");
	}
#endif
	USHORT size = MAX(l->data->w, l->data->h);
	Image *filled = create_image(size, size);
	USHORT x;
	USHORT y;
	for (x = 0; x < size; x++)
	{
		for (y = 0; y < size; y++)
		{
			if (x < l->data->w && y < l->data->h && getpixelbin(l->data, x, y))
				setpixel(filled, x, y, getpixel(img, x + l->rect.x, y + l->rect.y));
			else
				setpixel(filled, x, y,
				{
					255, 255, 255
				});
		}
	}
#ifndef BUILD_DEBUG
	free(l->data);
#endif
	l->data = get_resizedsquare(filled, 0, 0, size, size, w, h);
	free_image(filled);
}

OCRAPI void free_letter(Letter *l)
{
	if (is_realletter(l))
	{
		free_bimage(l->data);
#ifdef BUILD_DEBUG
		free_bimage(l->originaldata);
#endif
	}
	free(l);
}


static void adjust_block(Bin_Image *img, Rect *r)
{
	bool b;
	USHORT x;
	USHORT y;
	do
	{
		b = false;
		for (x = r->x; x < r->w + r->x; x++)
		{
			while (r->y && getpixelbin(img, x, r->y - 1))
			{
				r->y--;
				r->h++;
				b = true;
			}
			while (r->y + r->h < img->h && getpixelbin(img, x, r->y + r->h))
			{
				r->h++;
				b = true;
			}
		}
		for (y = r->y; y < r->h + r->y; y++)
		{
			while (r->x && getpixelbin(img, r->x - 1, y))
			{
				r->x--;
				r->w++;
				b = true;
			}

			while (r->x + r->w < img->w  && getpixelbin(img, r->x + r->w, y))
			{
				r->w++;
				b = true;
			}
		}
	} while (b);
}

OCRAPI Rect getrectcontaining_shape(Bin_Image *img, USHORT x, USHORT y)
{
#ifdef BUILD_DEBUG
	if (!getpixelbin(img, x, y))
	{
		critical("getrectcontaining_shape called with a non-black point");
	}
#endif
	Rect r;
	r.x = x;
	r.y = y;
	r.w = 1;
	r.h = 1;
	while (x + r.w + 1 < img->w &&
		getpixelbin(img, x + r.w + 1, y))
	{
		r.w++;
	}
	while (y + r.h + 1 < img->h &&
		getpixelbin(img, x, y + r.h + 1))
	{
		r.h++;
	}
	adjust_block(img, &r);
	return r;
}
