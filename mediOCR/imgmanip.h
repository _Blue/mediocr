/// File : imagmanip.h
/// Purpose : Contains fonctions to manages images data.

#ifndef IMGMANIP_H
#define IMGMANIP_H
#include "debugapi.h"
#include "stdafx.h"
#include "loadimg.h"

///Pixel access fonctions
Pixel getpixel(_in_ Image *img, USHORT x, USHORT y);
/* Retrieves pixel from image
 * Unsafe in release mode if x or y is invalid or if
 * Image data is invalid */

bool getpixelbin(_in_ Bin_Image *img, USHORT x, USHORT y);
//Retrieves pixel to a binarized image

BYTE getpixelgray(_in_ Gray_Image *img, USHORT x, USHORT y);
//Retrieves pixel from a gray image

Pixel getpixel_s(_in_ Image *, short x, short y, Pixel def);
/* returns def if x or y is out of bounds, returns getpixel otherwise */

Pixel getpixel_raw(_in_ Pixel *data, USHORT x, USHORT y, USHORT w);
/* returns pixel taken from raw data, this fonction is not safe*/

void setpixel(_inout_ Image *img, USHORT x, USHORT y, Pixel px);
/*Set pixel to an image
Safe if x, y and images are valid */

void setpixelrgb(_inout_ Image *img, USHORT x, USHORT y, BYTE R, BYTE G,
                 BYTE B);
// Same as above

void setpixelgray(_in_ Gray_Image *img, USHORT x, USHORT y, BYTE b);
// sets pixel in a gray image

void get_histogram(Gray_Image *img, int h[256]);
// sets the value of the histogram in h

void setpixelbin(_in_ Bin_Image *img, USHORT x, USHORT y, bool b);
// sets pixel in a binary image

void setpixel_s(_inout_ Image *img, short x, short y, Pixel px);
// sets pixels to an image, doesn't call critical if position is invalid

BYTE getpixel_asgray(_in_ Image *img, USHORT x, USHORT y);
//Retrieves gray value of a pixel in an image

///Image processing fonctions

/*
 * Returns a potential rotation angle
 */
float detect_skew(Gray_Image *img);

void rotate_image( _inout_ Image * img, float angle, USHORT originx,
                   USHORT originy);
/* rotates image off the specified angle (in radian )
note that the size of the image may change after rotating */

void remove_blank(_inout_ Bin_Image *img, _outopt_ USHORT *xoffset,
                  _outopt_ USHORT *yoffset);
/*Removes useless border from img, safe if img is valid, optional parameters
xoffset and yoffset give up and left padding (size of removed border)*/

Bin_Image *get_resizedsquare(_in_ Image *img, USHORT x, USHORT y, USHORT w,
                             USHORT h, USHORT neww, USHORT newh);
/* returns a binarized and resized version of the rectangle specified
(x,y,w,h)
safe if img is valid and w,h,neww and newwh are non zero.
This fonction provides minimal data loss during rezise.*/

void drawrect(_inout_ Image *img, USHORT x, USHORT y, USHORT w, USHORT h,
              Pixel color);
/* Draws a rect (borders only) on img in the specified color*/
void drawfromrect (_inout_ Image *img, Rect r, Pixel color);
/* Draws a rect (borders only) on img in the specified color*/

Rect mergerects(_in_ Rect r1,_in_ Rect r2);
/*Returns a rectangle that contains both r1 and r2*/

void prepareletter_fornn(_inout_ Letter *letter, USHORT w,USHORT h,_in_ Image *original);
/*Resizes letter for neural network, will backup old Image in debug mode*/

void addletter(_inout_ Letter *l1,_in_ Letter l2);
/*Adds l2 to l1*/
///Data query fonctions
bool islineblank(_in_ Bin_Image *img, USHORT line);
//returns true if the whole line is blank

bool iscolumnblank(_in_ Bin_Image *img, USHORT column);
//same for a column

Image *bimage_tosurface(_in_ Bin_Image *img);
// Converts binaryzed image into SDL_Surface, safe if img is valid
Image *gimage_to_image(_in_ Gray_Image *img);

Bin_Image *sub_surface(_in_ Bin_Image *img, USHORT x, USHORT y, USHORT w,
                       USHORT h);
//Returns surface defined by (x,y,x+w,y+h), safe if img is valid

Rect getrectcontaining_shape(_in_ Bin_Image *img,USHORT x,USHORT y);
//Returns rect containing shape starting at point (x,y)

///Memory cleaning fonctions
void free_bimage(_inout_ Bin_Image *);
// frees a bin_image

void free_grayimage(_inout_ Gray_Image *);
//Frees a grayscaled image

void free_image(_inout_ Image*);
//Frees an Image from memory

void free_letter(_inout_ Letter*);
//Frees a letter from memory

///Image creation fonctions
Bin_Image *create_bin_image(USHORT w, USHORT h);

Gray_Image *create_gray_image(USHORT w, USHORT h);

Image *create_image(USHORT w, USHORT h);

Point *createpoint(USHORT x,USHORT y);

#endif // IMAGEMANIPULATION_H
