#include "stdafx.h"
#include "letter.h"
#include "textextraction.h"



#define NBLETTER 26
#define NBLAYER 200
#define NBPOLICE 14
#define H 16
#define W 16

static struct network *g_n;

void start_lnn()
{
    g_n = load_network("letters.nn");
}

void quit_lnn()
{
    free_network(g_n);
}

void get_size(USHORT *h, USHORT *w)
{
    *h = H;
    *w = W;
}

double *get_letter()
{
    int i, j;
    char path[] = "tff/a/a.bmp";
    Image *img;
    Bin_Image *bimg;

    double *letter = (double*)malloc(sizeof(double)*NBLETTER*H*W*NBPOLICE);

    int h,w;
    for (j=0;j<NBPOLICE;j++)
    {
        for (i=0;i<NBLETTER;i++)
        {
            img = load_image(path);
            bimg = binarize(img, THRESHOLD, 127, 0, 0);

            for (h=0;h<H;h++)
            {
                for (w=0;w<W;w++)
                    letter[j*NBLETTER*H*W+i*H*W+h*W+w] = 
                        ((bimg->data[h*W+w] == 1) ? 1 : -1);
            }

            free_bimage(bimg);
            free_image(img);
            path[6]++;
            if (path[6] == 'z'+1)
                path[6] = 'A';
        }
        path[4]++;
        path[6] = 'a';
    }

    return letter;
}

void testL()
{
    int i,j;
    srand((UINT)time(NULL));

    int layer_size[] = {H*W, NBLAYER, NBLETTER};

    struct network *n = new_network(3, layer_size);

    double *letters = get_letter();

    double in[NBLETTER];

    char *buf = (char*)malloc(256);

    for (i=0;i<NBLETTER;i++)
        in[i] = -1;

    int k;

    for (i=0; i < 2000; i++)
    {
        for (j=0;j<NBPOLICE;j++)
        {
            for (k=0;k<NBLETTER;k++)
            {
                network_treatment(n,&letters[j*NBLETTER*H*W+k*W*H]);
                in[k] = 1;
                back_prop(n,in);
                in[k] = -1;
            }
        }
    }

    char *s = "letters.nn";
    save_network(n, s);

    int line,col;

    for (j=0;j<NBPOLICE;j++)
    {
        for (i=0; i<NBLETTER; i++)
        {
            for (line=0;line<H;line++)
            {
               for (col=0;col<W;col++)
               {
                    if (letters[j*NBLETTER*W*H+i*W*H+line*W+col] == 1)
                        putchar('O');
                    else putchar(' ');
               }
                printf("\n");
            }
            printf("\n");
        }
    }

    while(1)
    {
        printf("Type a new letter\n\n");
        double input[H*W];
        for (i = 0; i < H; i++)
        {
            for (j=0;j<W;j++)
                buf[j] = 0;
            if(!fgets(buf,256,stdin))
            {
                critical("fgets failed");
            }
            for (j=0;j<W;j++)
            {
                if (buf[j] == ' ' || buf[j] < '0') input[i*W+j] = -1;
                else input[i*W+j] = 1;
            }
        }
        network_treatment(n,input);
        double out[NBLETTER];
        get_outputs(n,out);
        printf("\n\nLetter:\n");
        int res = 0;
        for (i = 0; i < NBLETTER; i++)
        {
            printf("%c = %f\n",65+i,out[i]);
            if( out[res] < out[i]) res = i;
        }
        printf("\n\n");
        for (j = 0; j < H; j++)
        {
            for (col=0;col<W;col++)
            {
                 if (letters[res*H*W+j*W+col]== 1) putchar('O');
                 else putchar(' ');
            }
            putchar('\n');
        }
    }

    free(buf);
    free_network(n);
}

char letter(double *l, Letter *letter)
{
    int i, res;
    res=0;
    network_treatment(g_n,l);
    double out[NBLETTER];
    get_outputs(g_n,out);
    for (i=1;i<NBLETTER;i++)
    {
		if (out[res] <= out[i])
		{
			res = i;
		}
		if (res < 26)
		{
			letter->neuralresult['a' + i] = out[i];
		}
		else
		{
			letter->neuralresult['A' + i-26] = out[i];
		}

    }
    if (res<26)
        return 'a'+res;
    else return 'A'+res-26;
}

void get_sletter(Letter *l)
{
    double in[W*H];
    int i,j;
    for (i=0;i<H;i++)
    {
        for (j=0;j<W;j++)
        {
            if (getpixelbin(l->data, j, i))
            {
                in[i*W+j] = 1;
            }
            else
            {
                in[i*W+j] = -1;
            }
        }
    }
	l->value = letter(&in[0], l);//Save the best match in l->value and all values in  l->neuralvalues. Asserting the array is zeroized
}
