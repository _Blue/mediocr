#ifndef LETTER_H
#define LETTER_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "neurone.h"
#include "stdafx.h"
#include "imgmanip.h"

void get_size(_out_ USHORT *h,_out_ USHORT *w);
/*Return the size of the letter to pass to the NN*/

void get_sletter(Letter *l);
/*Letter recon*/

void start_lnn();
/*Init the NN*/

void quit_lnn();
/*End the NN*/

void testL();

#endif
