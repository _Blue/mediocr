#include "stdafx.h"
#include "loadimg.h"
#include "imgmanip.h"
#include <errno.h>
#define OVERWRITE_MSG "File : \"%s\" already exists, overwrite ?"

///Internal fonctions
static bool canwrite(_in_ const char *);
static Image *processbmpheader(_in_ BYTE* header, BYTE* bpp);
static bool loadbmp_1(_in_ FILE*, _inout_ Image*);
static bool loadbmp_8(_in_ FILE*, _inout_ Image*);
static bool loadbmp_16(_in_ FILE*, _inout_ Image*);
static bool loadbmp_24(_in_ FILE*, _inout_ Image*);
static bool loadbmp_32(_in_ FILE*, _inout_ Image*, bool);
static Image *load_image_internal(_in_ const char *path);
static bool canwrite(_in_ const char *);

static bool boverwrite = false;

typedef struct //Bitmap header
{
	char bfType[2];
	int bfSize;
	int bfReserved;
	int bfOffBits;
	int biSize;
	int biWidth;
	int biHeight;
	short biPlanes;
	short biBitCount;
	int biCompression;
	int biSizeImage;
	int biXPelsPerMeter;
	int biYPelsPerMeter;
	int biClrUsed;
	int biClrImportant;
} BmpHeader;



static bool canwrite(const char *path)
{
	if (boverwrite)
		return true;

	FILE *f = fopen(path, "r");
	if (!f)
		return true;
	fclose(f);
	char *msg = new char[strlen(path) + strlen(OVERWRITE_MSG) + 1];
	sprintf(msg, OVERWRITE_MSG, path);
	bool ret = MessageBox(NULL, msg, "mediOCR", MB_YESNO) == IDYES;
	delete[]msg;
	return ret;
}


OCRAPI void setoverwrite(bool b)
{
	boverwrite = b;
}

Image *load_image(Gdiplus::Bitmap *source)
{
	USHORT w = source->GetWidth();
	USHORT h = source->GetHeight();
	::Image *ret = create_image(w, h);
	USHORT x;
	USHORT y;
	Gdiplus::Color c;
	Pixel p;
	for (x = 0; x < w; x++)
	{
		for (y = 0; y < h; y++)
		{
			if (source->GetPixel(x, y, &c) != Gdiplus::Ok)
			{
				free_image(ret);
				return NULL;
			}
			p.red = c.GetRed();
			p.green = c.GetGreen();
			p.blue = c.GetBlue();
			setpixel(ret, x, y, p);
		}
	}
	return ret;
}

Image *processbmpheader(BYTE *header, BYTE *bpp)
{
	if (*(int*)header == 40 || *(int*)header == 52 || *(int*)header == 56
		|| *(int*)header == 108 || *(int*)header == 124)
	{
		if (*(int*)(header + 16) == 3)
			*bpp = 32;
		else if (*(int*)(header + 16) != 0)
		{
			printf("Warning : compressed bitmap are not supported");
			return NULL;
		}
		else
			*bpp = *(int*)(header + 14);
		Image *img = create_image(*(int*)(header + 4), *(int*)(header + 8));
		return img;
	}
	else if (*(int*)header == 14)
	{
		USHORT w, h;
		memcpy(&w, header + 2, 2);
		memcpy(&h, header + 2, 2);
		Image *img = create_image(*(int*)(header + 4), *(int*)(header + 8));
		*bpp = 24;
		return img;
	}
	return NULL;
}

Image *load_image(const char *path)
{
	Image *img = load_image_internal(path);
	if (img == NULL)
	{
		FILE *f = fopen(path, "r");
		if (f == NULL)
			return NULL;
		fclose(f);
		return false;
	}
	return img;
}

static Image *load_image_internal(const char *path)
{
	FILE *file = fopen(path, "rb");
	if (!file)
	{
		verbose("Failed to open file : \"%s\"\n", path);
		return NULL;
	}
	verbose("Opening : \"%s\"\n", path);
	int offset = 0;
	BYTE buffer[40]; //40=max header size
	BYTE bpp;
	if (!fread(buffer, 14, 1, file))
	{
		fclose(file);
		return NULL;
	}
	if (buffer[0] != 'B' || buffer[1] != 'M')
	{
		verbose("Invalid header\n");
		fclose(file);
		return NULL;
	}
	memcpy(&offset, &buffer[10], sizeof(int));
	if (!fread(buffer, 40, 1, file))
	{
		verbose("Failed to open file : \"%s\", failed to read header\n", path);
		fclose(file);
		return NULL;
	}
	Image *image = processbmpheader(buffer, &bpp);
	if (image == NULL)
	{
		verbose("Failed to open file : \"%s\", invalid header\n", path);
		fclose(file);
		return NULL;
	}
	verbose("Image size : %i * %i\n", image->w, image->h);
	fseek(file, offset, SEEK_SET);
	switch (bpp)
	{
	case 24:
		offset = loadbmp_24(file, image);
		break;
	case 32:
		offset = loadbmp_32(file, image, false);
		break;
	case 33:
		offset = loadbmp_32(file, image, true);
		break;
	case 16:
		offset = loadbmp_16(file, image);
		break;
	case 8:
		offset = loadbmp_8(file, image);
		break;
	case 1:
		offset = loadbmp_1(file, image);
		break;
	default:
		printf("Warning : unsupported bpp : %u", bpp);
		offset = false;
	}
	fclose(file);
	if (!offset)
	{
		verbose("Failed to load : \"%s\", unexpected file end\n", path);
		free_image(image);
		return NULL;
	}

	return image;
}

OCRAPI static bool loadbmp_32(FILE *file, Image *image, bool rgba)
{
	USHORT x;
	USHORT y;
	Pixel p;
	for (y = image->h - 1;; y--)
	{
		for (x = 0; x < image->w; x++)
		{
			if (!rgba)
				fseek(file, 1, SEEK_CUR);
			if (!fread(&p, sizeof(Pixel), 1, file))
			{
				return false;
			}
			setpixel(image, x, y, p);
			if (rgba)
				fseek(file, 1, SEEK_CUR);
		}
		if (y == 0)
			break;
	}
	return true;
}

OCRAPI static bool loadbmp_24(FILE *file, Image *image)
{
	USHORT x;
	USHORT y;
	USHORT w = (3 * (image->w + 1) / 4) * 4;
	Pixel p;
	for (y = image->h - 1;; y--)
	{
		for (x = 0; x < image->w; x++)
		{
			if (!fread(&p, sizeof(Pixel), 1, file))
			{
				return false;
			}
			setpixelrgb(image, x, y, p.blue, p.green, p.red);
		}
		fseek(file, w - (x * 3), SEEK_CUR);
		if (y == 0)
			break;
	}
	return true;
}

OCRAPI static bool loadbmp_16(FILE *file, Image *image)
{
	USHORT x;
	USHORT y;
	USHORT w = (2 * (image->w + 1) / 4) * 4;
	USHORT b;
	USHORT rmask = 0xF800;
	for (y = image->h - 1;; y++)
	{
		for (x = 0; x < image->w; x++)
		{
			if (!fread(&b, 2, 1, file))
			{
				return false;
			}
			setpixelrgb(image, x, y, (b & 0x1F), (b & 0x7E0) >> 5, (b & rmask) >> 11);
		}
		fseek(file, w - (x * 2), SEEK_CUR);
		if (y == 0)
			break;
	}
	return true;
}

OCRAPI static bool loadbmp_8(FILE *file, Image *image)
{
	printf("Warning 8 byte per pixel bitmaps are only supported with RRRGGGBB color set");
	USHORT x;
	USHORT y;
	USHORT w = (3 * (image->w + 1) / 4) * 4;
	BYTE b;
	for (y = image->h - 1;; y--)
	{
		for (x = 0; x < image->w; x++)
		{
			if (!fread(&b, 1, 1, file))
			{
				return false;
			}
			setpixelrgb(image, x, y, (b & 0x7) * 32,
				((b & 0x38) >> 3) * 32, (b >> 6) * 64);
		}
		fseek(file, w - (x * 3), SEEK_CUR);
		if (y == 0)
			break;
	}
	return true;
}

OCRAPI static bool loadbmp_1(FILE *file, Image *image)
{
	USHORT x;
	USHORT y;
	BYTE i;
	BYTE b;
	for (y = image->h - 1;; y--)
	{
		for (x = 0; x < image->w; x++)
		{
			if (!fread(&b, 1, 1, file))
			{
				return false;
			}
			for (i = 0; i < 8 && x < image->w; i++)
			{
				if (b & 1)
					setpixel(image, x, y, { 255, 255, 255 });
				else
					setpixel(image, x, y, { 0, 0, 0 });
				b >>= 1;
			}
		}
		if (y == 0)
			break;
	}
	return true;
}

bool save_image(Image *img, const char *path)
{
	if (!canwrite(path))
		return false;
	FILE *file = fopen(path, "wb");
	if (file == NULL)
	{
		verbose("Failed to write file : \"%s\"\n", path);
		return false;
	}
	USHORT x;
	USHORT y;

	BmpHeader bmph;
	USHORT linesize = (3 * (img->w + 1) / 4) * 4;
	BYTE *line = (BYTE*)malloc(sizeof(BYTE)*linesize);
	memcpy(bmph.bfType, "BM", 2);
	bmph.bfOffBits = 54;
	bmph.bfSize = bmph.bfOffBits + linesize * img->h;
	bmph.bfReserved = 0;
	bmph.biSize = 40;
	bmph.biWidth = img->w;
	bmph.biHeight = img->h;
	bmph.biPlanes = 1;
	bmph.biBitCount = 24;
	bmph.biCompression = 0;
	bmph.biSizeImage = linesize * img->h;
	bmph.biXPelsPerMeter = 0;
	bmph.biYPelsPerMeter = 0;
	bmph.biClrUsed = 0;
	bmph.biClrImportant = 0;

	if (!(fwrite(&bmph.bfType, 2, 1, file) &&
		fwrite(&bmph.bfSize, 4, 1, file) &&
		fwrite(&bmph.bfReserved, 4, 1, file) &&
		fwrite(&bmph.bfOffBits, 4, 1, file) &&
		fwrite(&bmph.biSize, 4, 1, file) &&
		fwrite(&bmph.biWidth, 4, 1, file) &&
		fwrite(&bmph.biHeight, 4, 1, file) &&
		fwrite(&bmph.biPlanes, 2, 1, file) &&
		fwrite(&bmph.biBitCount, 2, 1, file) &&
		fwrite(&bmph.biCompression, 4, 1, file) &&
		fwrite(&bmph.biSizeImage, 4, 1, file) &&
		fwrite(&bmph.biXPelsPerMeter, 4, 1, file) &&
		fwrite(&bmph.biYPelsPerMeter, 4, 1, file) &&
		fwrite(&bmph.biClrUsed, 4, 1, file) &&
		fwrite(&bmph.biClrImportant, 4, 1, file)))
	{
		verbose("fwrite failed while writting: \"%s\",  %i\n", path, ferror(file));
		fclose(file);
		return false;
	}
	ZeroMemory(line, linesize);
	for (y = img->h - 1;; y--)
	{
		for (x = 0; x < img->w; x++)
		{
			line[3 * x] = getpixel(img, x, y).blue;
			line[3 * x + 1] = getpixel(img, x, y).green;
			line[3 * x + 2] = getpixel(img, x, y).red;
		}
		if (fwrite(line, linesize, 1, file) != 1)
		{
			verbose("fwrite failed while writting: \"%s\", %i\n", path, ferror(file));
			fclose(file);
			return false;
		}
		if (y == 0)
			break;
	}
	fclose(file);
	return true;
}


Gdiplus::Bitmap *to_gdi(Image *source)
{
	Gdiplus::Bitmap *ret = new Gdiplus::Bitmap(source->w, source->h);
	USHORT x;
	USHORT y;
	Pixel p;
	Gdiplus::Color c;
	for (x = 0; x < source->w; x++)
	{
		for (y = 0; y < source->h; y++)
		{
			p = getpixel(source, x, y);
			c.SetFromCOLORREF(RGB(p.red, p.green, p.blue));
			ret->SetPixel(x, y, c);
		}
	}
	return ret;
}