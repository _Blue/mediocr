/// File : loadimg.h
/// Purpose : Contains fonctions used to load/save images from files

#ifndef LOADIMG_H
#define LOADIMG_H
#include "debugapi.h"
#include "stdafx.h"

///Load / save fonctions
::Image *load_image(_in_ Gdiplus::Bitmap *source);//Convert a Gdiplisbitmap to an Image *
::Image *load_image(_in_ const char *path);
Gdiplus::Bitmap *to_gdi(_in_ Image *source);

///Misc functons
void setoverwrite(bool);

#endif // IMAGEMANIPULATION_H
