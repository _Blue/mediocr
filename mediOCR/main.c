#include "ocrdefs.h"
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <math.h>
#include <gtk/gtk.h>
#include <time.h>

#include "textextraction.h"
#include "xor.h"
#include "letter.h"
#include "textexport.h"
#include "compute.h"
#include "guirc.h"
#include "spellcheck.h"

static void quit(GtkWidget *widget);
static void create_file_selection();
static void get_path(GtkWidget *bouton, GtkWidget *file_selection);
static void display_img(const char *link,bool noise,bool rotation, int binarization,int threshold,const char *dico,int threads,int rlsa1,int rlsa2);
static void result(void* data);
static void resize_img();
static gint preresize_img(GtkWindow *w, GdkEvent *event, gpointer data);
static void export_aspdf();
static void export_astxt();
static void writetext(const char *text);
static char *get_savedpath();
static void *resize_thread(void* data);
static void new_file();
static void about();

static bool loaded;
static bool bresize;
static GdkPixbuf *original;
static GtkDialog *ocrdialog;
static int last_resize;
static GtkBuilder *builder;

int main(int argc, char **argv)
{
    GError *error = NULL;
    gchar *filename = NULL;
    loaded = false;
    original = NULL;
    last_resize=0;
    setoverwrite(true);
    setverbose(true);
    gdk_threads_init();
    gtk_init(&argc, &argv);

    builder = gtk_builder_new();
    filename =  g_build_filename ("gui.glade", NULL);
    gtk_builder_add_from_file (builder, filename, &error);

    g_free (filename);

    if (error)
    {
        gint code = error->code;
        g_printerr("%s\n", error->message);
        g_error_free (error);
        return code;
    }

    controls[CTRL_WINDOW] = GTK_WIDGET(gtk_builder_get_object (builder, "window1"));
    controls[CTRL_TEXT] = GTK_WIDGET(gtk_builder_get_object(builder, "textview1"));
    controls[CTRL_HBOX] = GTK_WIDGET(gtk_builder_get_object(builder, "vbox1"));

    image = GTK_IMAGE(gtk_builder_get_object(builder, "image1"));
    ocrdialog=GTK_DIALOG(gtk_builder_get_object(builder, "dialog"));
    gtk_spin_button_set_range(GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "threshold")),1,254);
    gtk_spin_button_set_range(GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "threads")),1,64);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "threshold")),127);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "threads")),4);


    g_signal_connect(G_OBJECT(controls[CTRL_WINDOW]), "destroy", G_CALLBACK(quit),
                     NULL);
    g_signal_connect(G_OBJECT(GTK_WIDGET(gtk_builder_get_object(builder, "Load"))),
                     "activate", G_CALLBACK(create_file_selection), NULL);
    g_signal_connect(G_OBJECT(GTK_WIDGET(gtk_builder_get_object(builder, "new"))),
                     "activate", G_CALLBACK(new_file), NULL);
    g_signal_connect(G_OBJECT(GTK_WIDGET(gtk_builder_get_object(builder, "about"))),
                     "activate", G_CALLBACK(about), NULL);

    g_signal_connect(G_OBJECT(GTK_WIDGET(gtk_builder_get_object(builder,
                                         "exportpdf"))), "activate", G_CALLBACK(export_aspdf), NULL);
    g_signal_connect(G_OBJECT(GTK_WIDGET(gtk_builder_get_object(builder,
                                         "exporttxt"))), "activate", G_CALLBACK(export_astxt), NULL);
    g_signal_connect(G_OBJECT(controls[CTRL_WINDOW]), "configure_event",
                     G_CALLBACK(preresize_img), NULL);


    writetext("To load an image, go to File -> open");
    gtk_widget_show_all(controls[CTRL_WINDOW]);
    pthread_t thread;
    if(pthread_create(&thread,NULL,resize_thread,NULL))
    {
        critical("Failed to create resize thread");
    }
    gtk_main();
    return 0;
}

static gint preresize_img(GtkWindow *w, GdkEvent *event, gpointer data)
{
    UNUSED(data);
    UNUSED(event);
    UNUSED(w);
    if(original != NULL)
        bresize=true;
    return FALSE;
}


void create_file_selection()
{
    GtkWidget *selection = gtk_file_selection_new(
                               g_locale_to_utf8( "Choose an image", -1, NULL, NULL, NULL) );
    gtk_widget_show(selection);
    gtk_window_set_modal(GTK_WINDOW(selection), TRUE);

    g_signal_connect(G_OBJECT(GTK_FILE_SELECTION(selection)->ok_button), "clicked",
                     G_CALLBACK(get_path), selection );
    g_signal_connect_swapped(G_OBJECT(GTK_FILE_SELECTION(selection)->cancel_button),
                             "clicked", G_CALLBACK(gtk_widget_destroy), selection);
}

void get_path(GtkWidget *bouton, GtkWidget *file_selection)
{
    UNUSED(bouton);
    const char* path = gtk_file_selection_get_filename(GTK_FILE_SELECTION (file_selection));
    gtk_widget_destroy(file_selection);
    if(path==NULL)
        return;

    if(!gtk_dialog_run(ocrdialog))
    {
            gtk_widget_hide(GTK_WIDGET(ocrdialog));
            int bin;
            if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(builder, "sauvola"))))
            {
                bin=SAUVOLA;
            }
            else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(builder, "otsu"))))
            {
                bin=OTSU1;
            }
            else
            {
                bin=THRESHOLD;
            }
            char *dico=NULL;
            if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(builder, "frenchdico"))))
            {
                dico=malloc(strlen("dictionaries/french.dat")+1);
                strcpy(dico,"dictionaries/french.dat");
            }
            else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(builder, "englishdico"))))
            {
                dico=malloc(strlen("dictionaries/english.dat")+1);
                strcpy(dico,"dictionaries/english.dat");
            }

            display_img(path,
            gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(builder, "noise"))),
            gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gtk_builder_get_object(builder, "rotation"))),
            bin,
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "threshold")))
            ,dico,
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "threads"))),
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "rlsa1"))),
            gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "rlsa2"))));
    }
    gtk_widget_hide(GTK_WIDGET(ocrdialog));
}

static char *get_savedpath()
{
    GtkWidget *dialog = gtk_file_chooser_dialog_new("Export as pdf",
                        GTK_WINDOW(controls[CTRL_WINDOW]), GTK_FILE_CHOOSER_ACTION_SAVE,
                        GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                        NULL);
    gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
    char *ret = NULL;
    if(gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT)
    {
        ret = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
    }
    gtk_widget_destroy(dialog);
    return ret;
}

static void export_aspdf()
{
#ifdef NO_PDF
    GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(controls[CTRL_WINDOW]),
                        GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
                        "MediOCR was compiled without pdf support");
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
#else
    char *path = get_savedpath();
    if(path == NULL)
        return;
    GtkTextBuffer *buff = gtk_text_view_get_buffer(GTK_TEXT_VIEW(
                              controls[CTRL_TEXT]));
    PdfExportParams *p = get_defaultpdfparams();
    GtkTextIter begin;
    GtkTextIter end;
    gtk_text_buffer_get_start_iter(buff, &begin);
    gtk_text_buffer_get_end_iter(buff, &end);
    char *text = gtk_text_buffer_get_text(buff, &begin, &end, FALSE);
    if(!export_pdf(text, path, p))
    {
        GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(controls[CTRL_WINDOW]),
                            GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
                            "An error occured while exporting pdf");
        gtk_dialog_run(GTK_DIALOG(dialog));
        gtk_widget_destroy(dialog);
    }
    g_free(text);
    g_free(path);
    free(p);
#endif
}

static void export_astxt()
{
    char *path = get_savedpath();
    if(path == NULL)
        return;
    GtkTextBuffer *buff = gtk_text_view_get_buffer(GTK_TEXT_VIEW(
                              controls[CTRL_TEXT]));
    GtkTextIter begin;
    GtkTextIter end;
    gtk_text_buffer_get_start_iter(buff, &begin);
    gtk_text_buffer_get_end_iter(buff, &end);
    char *text = gtk_text_buffer_get_text(buff, &begin, &end, FALSE);
    if(!export_txt(text, path))
    {
        GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(controls[CTRL_WINDOW]),
                            GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
                            "An error occured while exporting txt file");
        gtk_dialog_run(GTK_DIALOG(dialog));
        gtk_widget_destroy(dialog);
    }
    g_free(text);
    g_free(path);
}

void display_img(const gchar *link,bool noise,bool rotation,int binarisation,int threshold,const char *dico,int threads, int rlsa1, int rlsa2)
{
    UNUSED(dico);
    GtkTextBuffer *buff = gtk_text_view_get_buffer(GTK_TEXT_VIEW(
                              controls[CTRL_TEXT]));//Erase text view
    gtk_text_buffer_set_text(buff, "", -1);

    if(original)
        g_object_unref(original);
    original = gdk_pixbuf_new_from_file(link, NULL);//Load image
    if(original==NULL)
    {
        GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(controls[CTRL_WINDOW]),
                            GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
                            "An error occured while reading file");
        gtk_dialog_run(GTK_DIALOG(dialog));
        gtk_widget_destroy(dialog);
        return;
    }
    resize_img();

    Extractfromimgparams *params = malloc(sizeof(Extractfromimgparams));
    params->colors = false;
    params->binarization = binarisation;
    params->threshold=threshold;
    params->maxthreads=threads;
    params->cb = &result;
    params->path = link;
    params->textout = writetext;
    params->noise=noise;
    params->rotation=rotation;
    params->rlsaprimaryradius=rlsa1;
    params->rlsafixeradius=rlsa2;
    params->dictionary=dico;
    extractfromimg(params);//Async begin
    loaded = true;
}

void result(void* data)
{
    gdk_threads_enter();
    if(original)
        g_object_unref(original);
    original=gdk_pixbuf_new_from_file("out.bmp", NULL);
    Extractfromimgresult *res = (Extractfromimgresult*)data;
    if(res->error||original==NULL)
    {
        GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(controls[CTRL_WINDOW]),
                            GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
                            "An error occured while reading file");
        gtk_dialog_run(GTK_DIALOG(dialog));
        gtk_widget_destroy(dialog);
        return;
    }
    bresize=true;
    char *text = malloc(sizeof(char) * res->lettercount + 1);
    USHORT i;
    for(i = 0; i < res->lettercount; i++)
    {
        text[i] = res->letters[i]->value;
    }
    text[i] = '\0';
    if(res->dico!=NULL)
    {
        text=apply_dictionary(text,res->dico);
    }
    GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(
                             controls[CTRL_TEXT]));
    gtk_text_buffer_set_text(buf, text, -1);
    free(text);
    gtk_text_view_set_buffer(GTK_TEXT_VIEW(controls[CTRL_TEXT]), buf);
    gtk_text_view_set_editable(GTK_TEXT_VIEW(controls[CTRL_TEXT]), TRUE);
    gdk_threads_leave();
}

void resize_img()
{
    if(time(NULL)-last_resize<1)
        return;
    last_resize=time(NULL);
    int w, h;
    GdkRectangle rect;
    gtk_widget_get_allocation(GTK_WIDGET(image), &rect);
    w = rect.width;
    h = rect.height;
    int pixbuf_h = gdk_pixbuf_get_height(original);
    int pixbuf_w = gdk_pixbuf_get_width(original);
    w-=10;
    h-=10;
    float ratio = pixbuf_w / (float)pixbuf_h;
    if (ratio >= w / (float)h)
    {
        pixbuf_w = w;
        pixbuf_h = w / ratio;
    }
    else
    {
        pixbuf_h = h;
        pixbuf_w = h * ratio;
    }
    GdkPixbuf *newp = gdk_pixbuf_scale_simple(original, pixbuf_w, pixbuf_h,
                      GDK_INTERP_BILINEAR);
    if(loaded)
        g_object_unref(gtk_image_get_pixbuf(image));
    gtk_image_set_from_pixbuf(image, newp);
    gtk_widget_queue_draw(GTK_WIDGET(image));
};

static void writetext(const char *text)
{
    gdk_threads_enter();
    GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(
                             controls[CTRL_TEXT]));
    GtkTextIter iter;
    gtk_text_buffer_get_end_iter(buf, &iter);
    gtk_text_buffer_insert(buf, &iter, text, -1);
    gdk_threads_leave();
}

static void new_file()
{
    gtk_image_set_from_pixbuf(image,NULL);
    GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(
                             controls[CTRL_TEXT]));
    gtk_text_buffer_set_text(buf, "To load an image, go to File -> open", -1);
    gtk_text_view_set_buffer(GTK_TEXT_VIEW(controls[CTRL_TEXT]), buf);
    gtk_text_view_set_editable(GTK_TEXT_VIEW(controls[CTRL_TEXT]), TRUE);
}

static void about()
{
    GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(controls[CTRL_WINDOW]),
                            GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
                            "MediOCR version 2.0\n Build date: "__DATE__", at: "__TIME__"\n Bardoux Antoine\n Barroyer Thibaut\n Boulay Pierre\n Lamatte Valentin\n For more infos, please visit: http://mediocr.net");
        gtk_dialog_run(GTK_DIALOG(dialog));
        gtk_widget_destroy(dialog);
}
void quit(GtkWidget* widget)
{
    gtk_widget_destroy(widget);
    gtk_main_quit();
}

void *resize_thread(void * data)
{
    UNUSED(data);
    while(true)
    {
        sleep(1);
        if(bresize)
        {
            gdk_threads_enter();
            resize_img();
            gdk_threads_leave();
        }
        bresize=false;
    }
    return 0;
}
