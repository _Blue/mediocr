#include "stdafx.h"
#include "mediOCR.h"
#include "compute.h"
#include "loadimg.h"
#include "textexport.h"

using namespace Gdiplus;
static INT_PTR CALLBACK Wndproc(HWND, UINT, WPARAM, LPARAM);
static INT_PTR CALLBACK Optionsproc(HWND, UINT, WPARAM, LPARAM);
static void Openfile(HWND parent);
static void Loaddictionaries(HWND);
static void Resultcallback(void *);
static void Imagereadycallback(void *);
static void Printc(const char *);
static void Adjustedit(HWND hwindow);//Adjust edit's size to fit the new window's size
static void Export(bool pdf, HWND hwindow);
static HWND hconsole;
static Bitmap *himage;
static HBITMAP hbitmap;
HINSTANCE hinstance;
CRITICAL_SECTION imageinuse;//To ensure that the currently shown image will not be modified while being used

int APIENTRY WinMain(_In_ HINSTANCE instance, _In_opt_ HINSTANCE hprevinstance, _In_ LPTSTR cmd, _In_ int nCmdShow)
{
	UNREFERENCED_PARAMETER(hprevinstance);
	UNREFERENCED_PARAMETER(cmd);
	hinstance = instance;
	himage = NULL;
	hbitmap = NULL;
	setoverwrite(true);
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);//Initializes Gdiplus

	InitializeCriticalSection(&imageinuse);

	DialogBox(instance, MAKEINTRESOURCE(IDD_MAIN), NULL, Wndproc);

	return 0;
}

static void Openfile(HWND parent)
{
	int curdirsize = GetCurrentDirectory(0, NULL);
	curdirsize++;//For the '\0'
	char *curdir = new char[curdirsize];
	GetCurrentDirectory(curdirsize, curdir);//Because GetOpenFileName can change the current directory, we must save it

	OPENFILENAME data;
	ZeroMemory(&data, sizeof(OPENFILENAME));
	data.hInstance = hinstance;
	data.lStructSize = sizeof(OPENFILENAME);
	data.lpstrFilter = "Images(bmp, jpg, png, gif)\0*.bmp;*.jpg;*.png;*.gif\0\0";
	data.Flags = OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_NONETWORKBUTTON;
	data.hwndOwner = parent;
	data.lpstrFile = (char*)malloc(65536);//Maximum file path size
	data.lpstrFile[0] = '\0';
	data.nMaxFile = 65536;
	data.lpstrTitle = "Open an image";
	if (!GetOpenFileName(&data))//if the user didn't click OK
	{
		free(data.lpstrFile);
		return;
	}

	if (!SetCurrentDirectory(curdir))//restore original directory
	{
		critical("SetCurrentDirectory failed, %i", GetLastError());
	}
	delete[]curdir;

	Extractfromimgparams *params = (Extractfromimgparams*)DialogBox(hinstance, MAKEINTRESOURCE(IDD_OPTIONS), parent, Optionsproc);
	if (params == NULL)
	{
		return;
	}

	params->hwindow = parent;
	SetWindowText(hconsole, "Opening \"");
	Printc(data.lpstrFile);
	Printc("\"\r\n");
	wchar_t *wpath = new wchar_t[strlen(data.lpstrFile) + 1];
	mbstowcs(wpath, data.lpstrFile, strlen(data.lpstrFile) + 1);

	EnterCriticalSection(&imageinuse);
	if (himage != NULL)
	{
		DeleteObject(hbitmap);
		delete himage;
	}

	himage = Gdiplus::Bitmap::FromFile(wpath);
	delete[]wpath;
	free(data.lpstrFile);

	if (himage->GetHBITMAP(NULL, &hbitmap) != Status::Ok)
	{
		delete himage;
		hbitmap = NULL;
		MessageBox(parent, "Failed to load image", "Error", MB_ICONERROR | MB_OK);
		return;
	}
	LeaveCriticalSection(&imageinuse);

	params->image = himage;
	params->imageready = Imagereadycallback;
	extractfromimg(params);
	EnableMenuItem(GetMenu(parent), ID_FILE_EXPORTASPDF, MF_DISABLED);
	EnableMenuItem(GetMenu(parent), ID_FILE_EXPORTASTXT, MF_DISABLED);
	EnableMenuItem(GetMenu(parent), IDM_OPEN, MF_DISABLED);
	Edit_SetReadOnly(GetDlgItem(parent, IDC_EDIT), true);
	RedrawWindow(parent, NULL, NULL, RDW_INVALIDATE);
}

static void Export(bool pdf, HWND hwindow)
{
	OPENFILENAME data;
	ZeroMemory(&data, sizeof(OPENFILENAME));
	data.hInstance = hinstance;
	data.lStructSize = sizeof(OPENFILENAME);
	if (pdf)
	{
		data.lpstrFilter = "Pdf file (*.pdf)\0*.pdf";
		data.lpstrDefExt = "pdf";
	}
	else
	{
		data.lpstrFilter = "Text file (*.txt)\0*.txt\0\0";
		data.lpstrDefExt = "txt";
	}
	data.Flags = OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_NONETWORKBUTTON | OFN_NOCHANGEDIR | OFN_OVERWRITEPROMPT;
	data.hwndOwner = hwindow;
	data.lpstrFile = (char*)malloc(65536);//Maximum file path size
	data.lpstrFile[0] = '\0';
	data.nMaxFile = 65536;
	data.lpstrTitle = "Export as txt";
	if (!GetSaveFileName(&data))//if the user didn't click OK
	{
		free(data.lpstrFile);
		return;
	}
	int l = GetWindowTextLength(GetDlgItem(hwindow, IDC_EDIT));
	l++;
	char *text = (char*)malloc(sizeof(char)*l);
	GetDlgItemText(hwindow, IDC_EDIT, text, l);
	bool error;
	if (pdf)
	{
#ifdef NO_PDF
		MessageBox(hwindow, "mediOCR was built without pdf support", "mediOCR", MB_OK);
#else
		PdfExportParams *p = get_defaultpdfparams();
		error = !export_pdf(text, data.lpstrFile, p);
		free(p);
#endif
	}
	else
	{
		error = !export_txt(text, data.lpstrFile);
	}	
	free(data.lpstrFile);
	free(text);

	if (error)
	{
		MessageBox(hwindow, "An error occured while writting file", "Error", MB_OK | MB_ICONERROR);
	}
}


INT_PTR CALLBACK Wndproc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	switch (message)
	{
	case WM_INITDIALOG:
	{
		hconsole = GetDlgItem(hwnd, IDC_EDIT);//Save the console handle, needed for log printing
		SetWindowText(hconsole, "To open an image, go to File -> Open");
		SendMessage(hwnd, WM_SETICON, ICON_SMALL, (LPARAM)LoadIcon(hinstance,MAKEINTRESOURCE(IDI_MEDIOCR)));
		return TRUE;
	}
	case WM_COMMAND:
	{
		switch (LOWORD(wparam))
		{
		case IDM_ABOUT:
		{
			MessageBox(hwnd, "mediOCR "VERSION"\nWindows build by BLUESCREEN\nBase on the original mediOCR (http://mediocr.net) project", "About", MB_ICONINFORMATION | MB_OK);
			break;
		}
		case IDM_OPEN:
		{
			Openfile(hwnd);
			break;
		}
		case ID_FILE_EXPORTASTXT:
		{
			Export(false, hwnd);
			break;
		}
		case ID_FILE_EXPORTASPDF:
		{
			Export(true, hwnd);
			break;
		}
		case IDM_EXIT: case IDCANCEL:
		{
			EndDialog(hwnd, FALSE);
			return TRUE;
		}
		default:
		{
			break;
		}
		}
		break;
	}
	case WM_PAINT:
	{
		if (hbitmap == NULL)
		{
			break;
		}
		BITMAP bm;
		PAINTSTRUCT ps;

		HDC hdc = BeginPaint(hwnd, &ps);
		SendMessage(hwnd, WM_ERASEBKGND, (LPARAM)hdc, 0);

		HDC hsrc = CreateCompatibleDC(hdc);
		EnterCriticalSection(&imageinuse);
		HBITMAP hbmold = (HBITMAP)SelectObject(hsrc, hbitmap);
		RECT rect;
		GetClientRect(hwnd, &rect);
		GetObject(hbitmap, sizeof(bm), &bm);
		StretchBlt(hdc, 0, 0, rect.right / 2, rect.bottom, hsrc, 0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);
		SelectObject(hsrc, hbmold);
		DeleteDC(hsrc);
		EndPaint(hwnd, &ps);
		LeaveCriticalSection(&imageinuse);
		break;
	}
	case WM_EXITSIZEMOVE://We redraw the image only when size change is finished to avoid consumming a lot of cpu resizing the image multiple time
	{
		RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE);//Force redraw when window size changed
		break;
	}
	case WM_SIZE:
	{
		Adjustedit(hwnd);
		break;
	}
	default:
	{
		break;
	}
	}
	return FALSE;
}

static void Resultcallback(void *data)//Called by the worker thread when processing is finished
{
	Extractfromimgresult *res = (Extractfromimgresult*)data;
	if (res->error)
	{
		MessageBox(NULL, "An error occured", "Error", MB_OK | MB_ICONERROR);
		return;
	}

	SetWindowText(hconsole, res->data);
	EnableMenuItem(GetMenu(res->hwindow), ID_FILE_EXPORTASPDF, MF_ENABLED);
	EnableMenuItem(GetMenu(res->hwindow), ID_FILE_EXPORTASTXT, MF_ENABLED);
	EnableMenuItem(GetMenu(res->hwindow), IDM_OPEN, MF_ENABLED);
	Edit_SetReadOnly(GetDlgItem(res->hwindow, IDC_EDIT), false);

	free(res->data);
	free(res);
}

static void Imagereadycallback(void *data)
{
	Imagereadycallbackdata *params = (Imagereadycallbackdata*)data;
	EnterCriticalSection(&imageinuse);
	DeleteObject(hbitmap);
	delete himage;
	himage = params->image;
	if (himage->GetHBITMAP(NULL, &hbitmap) != Status::Ok)
	{
		delete himage;
		himage = NULL;
		hbitmap = NULL;
	}
	LeaveCriticalSection(&imageinuse);
	RedrawWindow(params->hwindow, NULL, NULL, RDW_INVALIDATE);
	free(data);
}

static void Printc(const char *data)
{
	int index = GetWindowTextLength(hconsole);
	HWND focus = GetFocus();
	SetFocus(hconsole);
	SendMessage(hconsole, EM_SETSEL, (WPARAM)index, (LPARAM)index);
	SendMessage(hconsole, EM_REPLACESEL, 0, (LPARAM)data);
	SetFocus(focus);
}

static void Adjustedit(HWND hwindow)
{
	RECT rect;
	if (!GetClientRect(hwindow, &rect))
	{
		return;
	}

	MoveWindow(GetDlgItem(hwindow, IDC_EDIT), rect.right / 2 + 5, 5, rect.right / 2 - 10, rect.bottom - 10, TRUE);
}

static INT_PTR CALLBACK Optionsproc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	switch (message)
	{
	case WM_INITDIALOG:
	{
		Loaddictionaries(GetDlgItem(hwnd, IDC_DICTIONARY));
		Button_SetCheck(GetDlgItem(hwnd, IDC_SAUVOLA), true);

		//Set maximum spin boxes values
		SendMessage(GetDlgItem(hwnd, IDC_THRESHOLD_SPIN), UDM_SETRANGE, 0, MAKELPARAM(256, 0));
		SendMessage(GetDlgItem(hwnd, IDC_PRIMARYRLSA_SPIN), UDM_SETRANGE, 0, MAKELPARAM(UD_MAXVAL, 0));
		SendMessage(GetDlgItem(hwnd, IDC_SECONDARYRLSA_SPIN), UDM_SETRANGE, 0, MAKELPARAM(UD_MAXVAL, 0));

		//Set default spin boxes values
		SendMessage(GetDlgItem(hwnd, IDC_THRESHOLD_SPIN), UDM_SETPOS, 0, MAKELONG(127, 0));
		SendMessage(GetDlgItem(hwnd, IDC_PRIMARYRLSA_SPIN), UDM_SETPOS, 0, 0);
		SendMessage(GetDlgItem(hwnd, IDC_SECONDARYRLSA_SPIN), UDM_SETPOS, 0, 0);

		return (INT_PTR)TRUE;
	}
	case WM_NOTIFY:
	{
		NMUPDOWN *data = (NMUPDOWN*)lparam;
		if (data->hdr.code == UDN_DELTAPOS)//It means that a spin box has been changed
		{
			int value = LOWORD(SendMessage(data->hdr.hwndFrom, UDM_GETPOS, 0, 0)) + data->iDelta;//Because the actual spin control value if is the low order word
			if (value < 0 || (data->hdr.idFrom == IDC_THRESHOLD_SPIN&&value>255))
			{
				return 1;//if value is outside the bounds, returning 1 will prevent the new value to be applicated
			}
			int target;
			char text[MAX_PATH];
			ZeroMemory(text, MAX_PATH);
			if (data->hdr.idFrom == IDC_THRESHOLD_SPIN)
			{
				strcpy(text, "Threshold: ");
				target = IDC_THRESHOLD;
			}
			else if (data->hdr.idFrom == IDC_PRIMARYRLSA_SPIN)
			{
				strcpy(text, "Primary radius: ");
				target = IDC_PRIMARYRLSA;
			}
			else if (data->hdr.idFrom == IDC_SECONDARYRLSA_SPIN)
			{
				strcpy(text, "Secondary radius: ");
				target = IDC_SECONDARYRLSA;
			}

			if (value != 0 || target == IDC_THRESHOLD)
			{
				sprintf(text + strlen(text), "%i", value);
			}
			else
			{
				strcpy(text + strlen(text), "auto");
			}

			SetWindowText(GetDlgItem(hwnd, target), text);
			return 0;
		}
		break;
	}

	case WM_COMMAND:
	{
		int id = LOWORD(wparam);
		if (id == IDCANCEL)
		{
			EndDialog(hwnd, NULL);
			return TRUE;
		}
		if (id == IDOK || id == IDCANCEL)
		{
			Extractfromimgparams *params = (Extractfromimgparams*)malloc(sizeof(Extractfromimgparams));
			if (Button_GetCheck(GetDlgItem(hwnd, IDC_SAUVOLA)))
				params->binarization = SAUVOLA;
			else if (Button_GetCheck(GetDlgItem(hwnd, IDC_OTSU1)))
				params->binarization = OTSU1;
			else if (Button_GetCheck(GetDlgItem(hwnd, IDC_OTSU2)))
				params->binarization = OTSU2;
			else
			{
				params->binarization = THRESHOLD;
				params->threshold = LOWORD(SendMessage(GetDlgItem(hwnd, IDC_THRESHOLD_SPIN), UDM_GETPOS, 0, 0));
			}

			params->colors = Button_GetCheck(GetDlgItem(hwnd, IDC_COLORS)) == BST_CHECKED;
			params->rotation = Button_GetCheck(GetDlgItem(hwnd, IDC_ROTATION)) == BST_CHECKED;
			params->noise = Button_GetCheck(GetDlgItem(hwnd, IDC_NOISE)) == BST_CHECKED;

			params->maxthreads = 4;//Temporary default
			params->rlsafixeradius = LOWORD(SendMessage(GetDlgItem(hwnd, IDC_PRIMARYRLSA_SPIN), UDM_GETPOS, 0, 0));
			params->rlsaprimaryradius = LOWORD(SendMessage(GetDlgItem(hwnd, IDC_SECONDARYRLSA_SPIN), UDM_GETPOS, 0, 0));
			if (ComboBox_GetCurSel(GetDlgItem(hwnd, IDC_DICTIONARY)) != 0)
			{
				int l = ComboBox_GetTextLength(GetDlgItem(hwnd, IDC_DICTIONARY)) + 1;
				char *text = (char*)malloc(l + strlen(DICTIONARY_FOLDER) + 2);
				strcpy(text, DICTIONARY_FOLDER"\\");
				ComboBox_GetText(GetDlgItem(hwnd, IDC_DICTIONARY), text + strlen(text), l);
				params->dictionary = text;
			}
			else
			{
				params->dictionary = NULL;
			}

			params->cb = Resultcallback;
			params->textout = Printc;
			EndDialog(hwnd, (INT_PTR)params);

			return TRUE;
		}
		else if (id == IDC_THRESHOLD || id == IDC_SAUVOLA || id == IDC_OTSU1 || id == IDC_OTSU2)
		{
			EnableWindow(GetDlgItem(hwnd, IDC_THRESHOLD_SPIN), Button_GetCheck(GetDlgItem(hwnd, IDC_THRESHOLD)));
		}
		break;
	}
	}
	return (INT_PTR)FALSE;
}

static void Loaddictionaries(HWND hwnd)
{
	ComboBox_AddString(hwnd, "None");
	WIN32_FIND_DATA data;
	HANDLE hfile = FindFirstFile("dictionaries\\*.dat", &data);
	if (hfile == INVALID_HANDLE_VALUE)
	{
		ComboBox_SetCurSel(hwnd, 0);//If no dictionary found, set "none" as default choice
		return;
	}
	do
	{
		if (*data.cFileName != '.')
		{
			ComboBox_AddString(hwnd, data.cFileName);
		}
	} while (FindNextFile(hfile, &data));
	FindClose(hfile);

	ComboBox_SetCurSel(hwnd, 1);
}