#include "stdafx.h"
#include "neurone.h"

#define WEIGHT_COEFF (double)0.5
#define LEARN_COEFF (double)0.007;

void save_network(struct network *n, char *path)
{
	int i;
	FILE *f = fopen(path, "w");

	if (f == NULL)
	{
		critical("Cannot open %s\n", path);
		return;
	}

	fprintf(f, "%d\n", n->nb_layer);
	fprintf(f, "%d\n", n->nb_neurone);
	fprintf(f, "%d\n", n->nb_synapse);

	for (i = 0; i < n->nb_layer; i++)
	{
		fprintf(f, "%d\n", n->size_layer[i]);
	}
	for (i = 0; i < n->nb_neurone; i++)
	{
		fprintf(f, "%f\n", n->neurone[i]);
	}
	for (i = 0; i < n->nb_synapse; i++)
	{
		fprintf(f, "%f\n", n->synapse[i]);
	}

	fclose(f);
}

struct network *load_network(char *path)
{
	struct network *n = (struct network*)malloc(sizeof(struct network));
	int i, bufs = 0;
	FILE *f = fopen(path, "r");

	if (f == NULL)
	{
		critical("Cannot open %s\n", path);
		return NULL;
	}

	if (fscanf(f, "%d\n", &(n->nb_layer)) == 0 || fscanf(f, "%d\n", &(n->nb_neurone)) == 0 || fscanf(f, "%d\n", &(n->nb_synapse)) == 0)
		critical("Failed to read \"%s\"", path);

	n->size_layer = (int*)malloc(sizeof(int)*n->nb_layer);
	n->i_layer = (int*)malloc(sizeof(int) * n->nb_layer);

	for (i = 0; i < n->nb_layer; i++)
	{
		if (!fscanf(f, "%d\n", &(n->size_layer[i])))
			critical("Failed to read \"%s\"", path);
		n->i_layer[i] = bufs;
		bufs += n->size_layer[i];
	}

	n->neurone = (double*)malloc(sizeof(double)*n->nb_neurone);
	n->err_neurone = (double*)malloc(sizeof(double)*n->nb_neurone);

	for (i = 0; i < n->nb_neurone; i++)
	{
		if (!fscanf(f, "%lf\n", &(n->neurone[i])))
			critical("Failed to read \"%s\"", path);
	}

	n->size_layer_in = n->size_layer[0];
	n->size_layer_out = n->size_layer[n->nb_layer - 1];
	n->neurone_in = n->neurone;
	n->neurone_out = &n->neurone[n->i_layer[n->nb_layer - 1]];
	n->err_neurone_out = &n->err_neurone[n->i_layer[n->nb_layer - 1]];

	n->i_synapse = (int*)malloc(sizeof(int)*(n->nb_layer - 1));
	bufs = 0;
	for (i = 0; i < n->nb_layer - 1; i++)
	{
		n->i_synapse[i] = bufs;
		bufs += (n->size_layer[i] + 1)*n->size_layer[i + 1];
	}

	n->synapse = (double*)malloc(sizeof(double) * n->nb_synapse);
	for (i = 0; i < n->nb_synapse; i++)
	{
		if (!fscanf(f, "%lf\n", &(n->synapse[i])))
			critical("Failed to read \"%s\"", path);
	}

	return n;
}

struct network *new_network(int nb_layer, int *size_layer)
{
	struct network *n = (struct network *)malloc(sizeof(struct network));
	int i;

	n->nb_layer = nb_layer;
	n->size_layer = (int*)malloc(sizeof(int) * n->nb_layer);
	n->i_layer = (int*)malloc(sizeof(int) * n->nb_layer);

	n->nb_neurone = 0;
	for (i = 0; i < nb_layer; i++)
	{
		n->size_layer[i] = size_layer[i];
		n->i_layer[i] = n->nb_neurone;
		n->nb_neurone += size_layer[i];
	}

	n->neurone = (double*)malloc(sizeof(double) * n->nb_neurone);
	n->err_neurone = (double*)malloc(sizeof(double) * n->nb_neurone);

	n->size_layer_in = size_layer[0];
	n->size_layer_out = size_layer[nb_layer - 1];
	n->neurone_in = n->neurone;
	n->neurone_out = &n->neurone[n->i_layer[nb_layer - 1]];
	n->err_neurone_out = &n->err_neurone[n->i_layer[nb_layer - 1]];

	n->i_synapse = (int*)malloc(sizeof(int) * (n->nb_layer - 1));
	n->nb_synapse = 0;
	for (i = 0; i < nb_layer - 1; i++)
	{
		n->i_synapse[i] = n->nb_synapse;
		n->nb_synapse += (n->size_layer[i] + 1) * n->size_layer[i + 1];
	}

	n->synapse = (double*)malloc(sizeof(double) * n->nb_synapse);

	for (i = 0; i < n->nb_synapse; i++)
	{
		n->synapse[i] = WEIGHT_COEFF * (double)rand() / RAND_MAX
			- WEIGHT_COEFF / 2;

	}
	return n;
}

void free_network(struct network *n)
{
	free(n->size_layer);
	free(n->i_layer);
	free(n->neurone);
	free(n->err_neurone);
	free(n->i_synapse);
	free(n->synapse);
	free(n);
}


void network_treatment(struct network *n, double *inputs)
{
	int i, j, k, i_synapse = 0;
	double s;

	for (i = 0; i < n->size_layer_in; i++)
		n->neurone_in[i] = inputs[i];

	for (i = 1; i < n->nb_layer; i++)
	{
		for (j = n->i_layer[i]; j < n->i_layer[i] + n->size_layer[i]; j++)
		{
			s = 0.0;
			for (k = n->i_layer[i - 1];
				k < n->i_layer[i - 1] + n->size_layer[i - 1]; k++)
			{
				s += n->neurone[k] * n->synapse[i_synapse];
				i_synapse++;
			}
			s += n->synapse[i_synapse];
			i_synapse++;
			n->neurone[j] = s;
			if (i != n->nb_layer - 1)
				n->neurone[j] = tanh(n->neurone[j]);
		}
	}
}


void get_outputs(struct network *n, double *outputs)
{
	int i;
	for (i = 0; i < n->size_layer_out; i++)
	{
		outputs[i] = n->neurone_out[i];
	}
}

void back_prop(struct network *n, double *outputs)
{
	int i, j, jj, k, i_synapse2;
	int i_synapse = n->i_synapse[n->nb_layer - 2];
	double new_weight;

	for (i = 0; i < n->size_layer_out; i++)
	{
		n->err_neurone_out[i] = n->neurone_out[i] - outputs[i];
		for (j = n->i_layer[n->nb_layer - 2]; j < n->i_layer[n->nb_layer - 2] +
			n->size_layer[n->nb_layer - 2]; j++)
		{
			new_weight = n->err_neurone_out[i] * n->neurone[j] * LEARN_COEFF;
			n->synapse[i_synapse] -= new_weight;

			if (n->synapse[i_synapse] > 5) n->synapse[i_synapse] = 5;
			if (n->synapse[i_synapse] < -5) n->synapse[i_synapse] = -5;

			i_synapse++;
		}
		new_weight = n->err_neurone_out[i] * LEARN_COEFF;
		n->synapse[i_synapse] -= new_weight;

		if (n->synapse[i_synapse] > 5)
			n->synapse[i_synapse] = 5;
		if (n->synapse[i_synapse] < -5)
			n->synapse[i_synapse] = -5;

		i_synapse++;
	}


	for (i = n->nb_layer - 2; i > 0; i--)
	{
		jj = 0;
		i_synapse = n->i_synapse[i - 1];

		for (j = n->i_layer[i]; j < n->i_layer[i] +
			n->size_layer[i]; j++, jj++)
		{
			i_synapse2 = n->i_synapse[i] + jj;
			n->err_neurone[j] = 0;

			for (k = n->i_layer[i + 1]; k < n->i_layer[i + 1] +
				n->size_layer[i + 1]; k++)
			{
				n->err_neurone[j] += n->synapse[i_synapse2] *
					n->err_neurone[k];
				i_synapse2 += n->size_layer[i] + 1;
			}

			for (k = n->i_layer[i - 1]; k < n->i_layer[i - 1] +
				n->size_layer[i - 1]; k++)
			{
				new_weight = 1.0 - n->neurone[j] * n->neurone[j];
				new_weight *= n->err_neurone[j] * LEARN_COEFF;
				new_weight *= n->neurone[k];

				n->synapse[i_synapse] -= new_weight;
				i_synapse++;
			}

			new_weight = 1.0 - n->neurone[j] * n->neurone[j];
			new_weight *= n->err_neurone[j] * LEARN_COEFF;
			n->synapse[i_synapse] -= new_weight;
			i_synapse++;
		}
	}
}
