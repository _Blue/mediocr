#ifndef NEURONE_H
#define NEURONE_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "debugapi.h"

struct network
{
    double *synapse;

    double *neurone;
    double *neurone_in;
    double *neurone_out;

    double *err_neurone;
    double *err_neurone_out;

    int *i_layer;
    int *i_synapse;

    int nb_layer;
    int nb_neurone;
    int nb_synapse;

    int *size_layer;
    int size_layer_in;
    int size_layer_out;
};

struct network *new_network(int nb_layer, int *size_layer);
/*Create a new network                           */
/*Param 1: nb_layer, an amout of layer           */
/*Param 2: size_layer, a vector of size for      */
/*each layer                                     */

void free_network(struct network *n);
/*Free a network                                 */
/*Param 1: n, the network to free                */

void back_prop(struct network *n, double *outputs);
/*Apply the backpropagation algorithm            */
/*Param 1: n, the learning network               */
/*Param 2: ouputs, a vector of desired outputs   */

void get_outputs(struct network *n, double *outputs);
/*Putting netxork treatment in outputs           */
/*Param 1: n, the network to get value           */
/*Param 2: outputs                               */

void network_treatment(struct network *n, double *inputs);
/*Treating the network                           */
/*Param 1: n, the network to treat               */
/*Param 2: inputs, inputs for the treatment      */

void save_network(struct network *n, char *path);
/*Saving the network                             */
/*Param 1: n, the network to save                */
/*Param 2: path, the path of the save file       */

struct network *load_network(char *path);
/*Loadin the network                             */
/*Param 1: path, the path of the save file       */

#endif
