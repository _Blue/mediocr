#include "stdafx.h"
#include "queue.h"

OCRAPI Queue *createqueue()
{
    Queue *ret = (Queue*)malloc(sizeof(Queue));
    ZeroMemory(ret, sizeof(Queue));
    return ret;
}

OCRAPI void push(Queue *s, void *d)
{
    s->count++;
    if(s->last == NULL) //Must be the first one
    {
        s->last = (QueueNode*)malloc(sizeof(QueueNode));
        s->last->data = d;
        s->first = s->last;
        s->first->next = NULL;
        s->first->previous = NULL;
        s->last->queue = s;
        return;
    }
    s->last->next =(QueueNode*) malloc(sizeof(QueueNode));
    s->last->next->queue = s;
    ((QueueNode*)s->last->next)->data = d;
    ((QueueNode*)s->last->next)->previous = s->last;
    s->last = s->last->next;
    s->last->next = NULL;
}

OCRAPI void *pop(Queue *s)
{
#ifdef BUILD_DEBUG
    if(isqueueempty(s))
    {
        critical("Pop called on an empty queue");
    }
#endif
    QueueNode *node = s->first;
    void *ret = node->data;
    s->first = node->next;
    s->count--;
    if(s->count == 0)
    {
        s->last = NULL;
    }
    else
    {
        s->first->previous = NULL;
    }
    free(node);
    return ret;
}

OCRAPI void queue_insertafter(QueueNode *node, void *data)
{
#ifdef BUILD_DEBUG
    if(node->next == NULL)
    {
        critical("queue_insertafter called with a last node");
        return;
    }
#endif
    QueueNode *newnode = (QueueNode*)malloc(sizeof(QueueNode));
    newnode->next = node->next;
    newnode->previous = node;
    node->next->previous = newnode;
    node->next = newnode;
    newnode->data = data;
    newnode->queue = node->queue;
    ((Queue*)node->queue)->count++;
}

OCRAPI void queue_insertbefore(QueueNode *node, void *data)
{
#ifdef BUILD_DEBUG
    if(node->previous == NULL)
    {
        critical("queue_insertbefore called with a first node");
        return;
    }
#endif
    QueueNode *newnode = (QueueNode*)malloc(sizeof(QueueNode));
    newnode->next = node;
    newnode->previous = node->previous;
    node->previous->next = newnode;
    node->previous = newnode;
    newnode->data = data;
    newnode->queue = node->queue;
    ((Queue*)node->queue)->count++;
}

OCRAPI void queue_remove_node(QueueNode *q)
{
    if(((Queue*)q->queue)->first == q)
    {
        ((Queue*)q->queue)->first = q->next;
    }
    if(((Queue*)q->queue)->last == q)
    {
        ((Queue*)q->queue)->first = q->previous;
    }
    if(q->previous != NULL)
    {
        q->previous->next = q->next;
    }
    if(q->next != NULL)
    {
        q->next->previous = q->previous;
    }
    ((Queue*)q->queue)->count--;
    free(q);
}


OCRAPI bool isqueueempty(Queue *s)
{
    return s->count == 0;
}

OCRAPI USHORT queue_count(Queue *s)
{
    return s->count;
}

OCRAPI void freequeue(Queue *q)
{
#ifdef BUILD_DEBUG
    if(!isqueueempty(q))
        printf("Warning : deleting"
               "non-empty queue (%u elements )", queue_count(q));
#endif
    free(q);
}
