/// queue.h
///Purpose : queue/linkedlist management
///WARNING : This stack is NOT concurrent
///Last modified by : BLUESCREEN

#ifndef QUEUE_H
#define QUEUE_H
#include "stdafx.h"
struct Queue;
struct QueueNode
{
    struct QueueNode *previous;
    struct QueueNode *next;
    void *queue;
    void *data;
};

struct Queue
{
    QueueNode *first;
    QueueNode *last;
    USHORT count;
};

Queue *createqueue();
//Creates a queue

void push(_inout_ Queue*, _in_ void *data);
//adds an element at the end

void *pop(_inout_ Queue*);
//deletes last element from stack and returns it

void freequeue(_inout_ Queue*);
//deletes stack and all elements

bool isqueueempty(_in_ Queue*);
//returns true if stack is empty

void queue_insertafter(QueueNode *q, void *data);
//Adds data after q in a queue

void queue_insertbefore(QueueNode *q,void *data);
//Adds data before q in a queue

void queue_remove_node(QueueNode *q);
//Removes q from the queue

USHORT queue_count(_in_ Queue*);
//returns elements count from Stack


#endif
