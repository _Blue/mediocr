//{{NO_DEPENDENCIES}}
// Fichier Include généré de Microsoft Visual C++.
// Utilisé par mediOCR.rc
//
#define IDC_MYICON                      2
#define IDD_MEDIOCR_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MEDIOCR                     107
#define IDI_SMALL                       108
#define IDC_MEDIOCR                     109
#define IDR_MAINFRAME                   128
#define IDD_MAIN                        129
#define IDD_OPTIONS                     130
#define IDC_EDIT                        1001
#define IDC_THRESHOLD                   1002
#define IDC_OTSU1                       1003
#define IDC_OTSU2                       1004
#define IDC_SAUVOLA                     1005
#define IDC_THRESHOLD_SPIN              1008
#define IDC_PRIMARYRLSA                 1009
#define IDC_PRIMARYRLSA_SPIN            1010
#define IDC_SECONDARYRLSA_SPIN          1011
#define IDC_COLORS                      1012
#define IDC_NOISE                       1013
#define IDC_ROTATION                    1014
#define IDC_DICTIONARY                  1015
#define IDC_SECONDARYRLSA               1016
#define ID_FICHIER_OPEN                 32771
#define ID_OPEN                         32772
#define IDM_OPEN                        32773
#define ID_FILE_EXPORTASTXT             32774
#define ID_FILE_EXPORTASPDF             32775
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
