#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "spellcheck.h"


static char *load_dico(const char *path);
static char *check_word(const char *word, const char *dico, Letter **letters);
static bool get_nextword(const char **data, char *word, USHORT size);
static float word_match(const char *word, Letter **data);

static float word_match(const char *word, Letter **data)//Uses the values computed by the neural network to calculate match probability
{
	float res = 0;
	while (*word != '\0')
	{
		if ((*word >= 'a' && *word <= 'z') || (*word >= 'A' && *word <= 'Z'))//in case of special char, like "-, �, �, etc"
		{
			res += (float)(*data)->neuralresult[*word];
		}
		data++;
		word++;
	}
	return res;
}

static bool get_nextword(const char **data, char *word, USHORT max)//Fills word with the next word in the dictionary. Returns true if there is more data
{
	BYTE size = 0;
	char *wordb = word;
	while (**data != '\n'&&**data != '\0')
	{
		if (size == max)
		{
			while (**data != '\n'&&**data != '\0')
			{
				(*data)++;
			}
			size = 0;
			if (**data != '\0')
			{
				(*data)++;
			}
			word = wordb;
			continue;
		}
		*word = **data;
		word++;
		(*data)++;
		size++;
	}
	*word = '\0';
	bool b = **data != '\0';
	(*data)++;
	return b;
}

char *check_word(const char *word, const char *dico, Letter **letters)
{
	char *best = (char*)malloc(strlen(word) + 2);
	strcpy(best, word);
	float fbest = 0;
	int wlen = strlen(word);
	char *data = (char*)malloc(wlen + 2);
	float value;
	if (wlen != 1)
	{
		while (get_nextword(&dico, data, strlen(word) + 1))
		{
			if (strlen(data) == wlen)
			{
				value = word_match(data, letters);
				if (value > fbest)
				{
					fbest = value;
					strcpy(best, data);
				}
			}
		}
	}
	free(data);
	if (fbest < (word_match(word, letters) / 2))
		strcpy(best, word);
	return best;
}

static char *load_dico(const char *path)
{
	FILE *f = fopen(path, "rb");//Because "r" may corrupt the value of ftell
	if (f == NULL)
	{
		verbose("Failed to open dictionary : \"%s\"\r\n", path);
		return NULL;
	}
	fseek(f, 0, SEEK_END);
	unsigned long size = ftell(f);
	char *data = (char*)malloc(sizeof(char)*(size + 1));
	fseek(f, 0, SEEK_SET);
	data[size] = '\0';
	if (!fread(data, sizeof(char), size, f))
	{
		verbose("Failed to read dictionary : \"%s\"\r\n", path);
		fclose(f);
		free(data);
		return NULL;
	}
	char *i = data;
	char *c = data;
	i++;
	c++;
	while (*c != '\0')//This loop replaces "\r\n" with "\n"
	{
		c++;
		*i = *c;
		if (*i != '\r')
		{
			i++;
		}
	}
	*i = '\0';
	verbose("Loaded \"%s\", %li bytes\r\n", path, size);
	return data;
}

char *apply_dictionary(char *text, const char *dico, Letter **letters)
{
	char *dicodata = load_dico(dico);
	char *begin;
	USHORT count = 0;
	if (dicodata == NULL)
	{
		verbose("Failed to load dictionary: \"%s\"\r\n", dico);
		return NULL;
	}
	Queue *q = createqueue();
	char c = '\0';
	int len;
	int letteroffset;
	while (*text != '\0')
	{
		begin = text;
		len = strlen(begin);
		letteroffset = 0;
		while (*text != ' '&&*text != '\0'&&*text != '\n')
		{
			text++;
			letteroffset++;
		}
		c = *text;
		*text = '\0';
		if (len < 250 && len > 1)
		{
			char *ret = check_word(begin, dicodata, letters);
			push(q, ret);
			count += strlen(ret) + 2;
		}
		else
		{
			char *d = (char *)calloc(len + 2, 1);
			strcpy(d, begin);
			push(q, d);
			count += len + 2;
		}
		if (c != '\0')//if there were a line feed, we must keep it
		{
			*text = c;
			text++;
			letters += letteroffset + 1;
			char *t = (char*)malloc(3);
			if (c == '\n')
			{
				strcpy(t, "\r\n");
			}
			else
			{
				t[0] = c;
				t[1] = '\0';
			}
			push(q, t);
		}
	}
	free(dicodata);
	char *ret = (char*)malloc(count + 1);
	char *cur;
	ZeroMemory(ret, count + 1);
	while (!isqueueempty(q))
	{
		cur = (char*)pop(q);
		strcpy(ret + strlen(ret), cur);
		free(cur);
	}
	freequeue(q);
	return ret;
}

UINT min3(UINT a, UINT b, UINT c)
{
	if (a >= b)
	{
		if (a > c)
			return a;
		else
			return c;
	}
	else
	{
		if (b > c)
			return b;
		else
			return c;
	}
}