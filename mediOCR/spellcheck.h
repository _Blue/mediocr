#ifndef SPELLCHECK_H
#define SPELLCHECK_H

#include "debugapi.h"
#include "queue.h"

char *apply_dictionary(char *data, const char *dico, Letter **letters);
#endif
