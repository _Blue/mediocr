/// File : stdafx.h
///Purpose :  Defines global constants and
///structures for the whole project
///It should be included by any file

#pragma once

#ifndef OCRDEFS_H
#define OCRDEFS_H
#include "targetver.h"

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <objidl.h>
#include <gdiplus.h>
#include <commdlg.h>
#include <commctrl.h>
#include <windowsx.h>
#include <cderr.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define _in_
#define _out_
#define _inout_
#define _opt_
#define _inopt_
#define _outopt_
#define _inoutopt_
#define FILE_BUFFER 4096
#define MASK_RED 0xff0000
#define MASK_GREEN 0x00ff00
#define MASK_BLUE 0x0000ff
#define GAUSS_BLUR_RADIUS 1
#define MIN_ACCENT_COMON_RATION 0.3
#define LOG_FILE "logs.txt"
#define DBG_MAX_FRAMES 50
#define DBG_MAX_CRITICAL_MESSAGE 1024
#define MAX_VERBOSEMSG 512
#define DEFAULT_MAXTHREADS 4
#define DEFAULT_NN_MAXTHREADS 1
#define WORD_SPACE_MIN_RATIO 0.6
#define THRESHOLD 0
#define OTSU1 1
#define OTSU2 2
#define SAUVOLA 3
#define DICTIONARY_FOLDER "dictionaries"
#ifdef NO_PDF
#define VERSION "1.0-win (no pdf support)"
#else
#define VERSION "1.0-win (with pdf support)"
#endif
#ifdef BUILD_RELEASE
#define OCRAPI 
#else
#ifdef BUILD_DEBUG
#define OCRAPI
#include "debugapi.h"
#else
#error target build not specified by define (BUILD_DEBUG/BUILD_RELEASE)
#endif
#endif

#define DEFAULT_LETTERCOLOR {0, 255, 0}
#define DEFAULT_LINECOLOR {255, 0, 0}
#define DEFAULT_BLOCKCOLOR {0, 0, 255}
#define DEFAULT_SPACECOLOR {255,0,255}
#define DEFAULT_INTERBLOCKCOLOR {255,255,0}
#define RLSA_C 6
#define LINE_RLSA 15
#define OPERATION_OCR 0
#define OPERATION_ROTATE 1
#define OPERATION_BINARIZE 2
#define OPERATION_RSLA 3
#define OPERATION_DRAW 4
#define UNUSED(x) (void)(x);

typedef unsigned short USHORT;
typedef unsigned int UINT;
typedef unsigned long DWORD;
typedef unsigned char BYTE;
int toint(float);
float MAXF(float, float);
float MINF(float, float);
int MAX(int, int);
int MIN(int, int);
float ABS(float);

/**
* \struct Pixel
* Stores the components of a pixel in RGB
*/
typedef struct
{
	BYTE red;
	BYTE green;
	BYTE blue;
} Pixel;

/**
* \struct Gray_Image
* Stores an image in grayscale
*/
typedef struct
{
	USHORT w;
	USHORT h;
	BYTE *data;
} Gray_Image;

/*
* \struct Bin_Image
* Stores a binarized image
*/
typedef struct
{
	USHORT w;
	USHORT h;
	bool *data;
} Bin_Image;

/*
* \struct Image
* Stores an image in RGB, composed of an array of Pixel
*/
typedef struct
{
	USHORT w;
	USHORT h;
	Pixel *data;
}Image;

/*
* \struct Rect
*/
typedef struct
{
	USHORT x;
	USHORT y;
	USHORT w;
	USHORT h;
} Rect;

bool rect_equals(Rect r1, Rect r2);

/*
* \struct Letter
*/
typedef struct
{
	Rect rect;
	Pixel color;
	Bin_Image *data;
#ifdef BUILD_DEBUG
	Bin_Image *originaldata;
#endif
	Rect block;
	char value;//The best key value
	double neuralresult[255];
} Letter;

/*
* \struct Point
*/
typedef struct
{
	USHORT x;
	USHORT y;
} Point;

typedef struct
{
	Rect rect;
	Rect block;
} Line;

#endif // OCRDEFS_H

