#include "stdafx.h"
#include "textexport.h"
#include "debugapi.h"
#ifndef NO_PDF
#include <setjmp.h>
#include <hpdf.h>
#endif

#ifndef NO_PDF
jmp_buf env;
static void error_handler(HPDF_STATUS error_no, HPDF_STATUS detail_no,
	void *user_data);
#endif
OCRAPI bool export_txt(const char *text, const char *path)
{
	FILE *file = fopen(path, "w");
	if (!file)
	{
		printf("Failed export text, failed to open : \"%s\"\n", path);
		return false;
	}
	if (!fwrite(text, sizeof(char), strlen(text), file))
	{
		printf("Error while writing : \"%s\"\n", path);
		fclose(file);
		return false;
	}
	fclose(file);
	return true;
}

#ifndef NO_PDF
bool export_pdf(const char *data, const char *path,PdfExportParams *params)
{
	HPDF_Doc pdf;
	HPDF_Page page;
	HPDF_Font def_font;
	HPDF_REAL tw;
	HPDF_REAL height;
	HPDF_REAL width;
	pdf = HPDF_New (error_handler, NULL);
	if (!pdf)
	{
		verbose ("Failed to create PdfDoc object\n");
		return false;
	}
	/*if (setjmp(env)) //Why is this commented ? 
	{
	HPDF_Free (pdf);
	return false;
	}*/

	page = HPDF_AddPage (pdf);
	height = HPDF_Page_GetHeight (page);
	width = HPDF_Page_GetWidth (page);

	HPDF_Page_SetLineWidth (page, params->linesize);

	def_font = HPDF_GetFont (pdf, params->font, NULL);
	HPDF_Page_SetFontAndSize (page, def_font, params->titlesize);
	tw = HPDF_Page_TextWidth (page, params->title);
	HPDF_Page_BeginText (page);
	HPDF_Page_TextOut (page, (width - tw) / 2, height - 50, params->title);
	HPDF_Page_EndText (page);

	HPDF_Page_BeginText (page);
	HPDF_Page_SetFontAndSize (page, def_font, params->subtitlesize);
	HPDF_Page_TextOut (page, params->margin, height - 80, params->subtitle);
	HPDF_Page_EndText (page);

	HPDF_Page_BeginText (page);
	HPDF_Page_MoveTextPos (page, params->margin, height - 105);
	HPDF_Page_SetFontAndSize (page, def_font, params->size);
	char *buf = (char*)malloc(sizeof(char) * (strlen(data) + 1));
	ZeroMemory(buf, strlen(data) + 1);
	int bufpos = 0;
	while(data[bufpos] != '\0')
	{
		while(HPDF_Page_TextWidth(page, buf) < width - params->margin * 2)
		{
			if(data[bufpos] == '\n')
			{
				break;
			}
			buf[bufpos] = data[bufpos];
			if(data[bufpos] == '\0')
				break;
			bufpos++;
		}
		HPDF_Page_ShowText(page, buf);
		if(data[bufpos] == '\n')
		{
			HPDF_Page_MoveTextPos (page, 0, (HPDF_REAL)-params->spacebetweenparagraphs);
			bufpos++;
		}
		else
		{
			HPDF_Page_MoveTextPos(page, 0, (HPDF_REAL)-params->spacebetweenlines);
		}
		if(HPDF_Page_GetCurrentTextPos(page).y <= 0)
		{
			HPDF_Page_EndText(page);
			page = HPDF_AddPage(pdf);
			HPDF_Page_SetLineWidth (page, params->linesize);
			HPDF_Page_SetFontAndSize (page, def_font, params->size);
			HPDF_Page_BeginText(page);
			HPDF_Page_MoveTextPos(page, params->margin, height - 50);
		}
		data += bufpos;
		memset(buf, 0, strlen(data) + 1);
		bufpos = 0;
	}
	if(HPDF_SaveToFile (pdf, path))
	{
		HPDF_Free (pdf);
		verbose("HPDF_SaveToFile failed, failed to save: \"%s\"\n", path);
		return false;
	}
	HPDF_Free (pdf);
	return true;
}

static void error_handler (HPDF_STATUS error_no, HPDF_STATUS detail_no,
	void *user_data)
{
	critical("HPDF internal error, code: %u, detail: %u, data: %x", error_no,detail_no, user_data);
	longjmp(env, 1);
}

OCRAPI PdfExportParams *get_defaultpdfparams()
{
	PdfExportParams *p = (PdfExportParams *) malloc((sizeof(PdfExportParams)));
	p->font = "Courier";
	p->linesize = 1;
	p->size = 12;
	p->title = "MediOCR";
	p->subtitle = "mediocr.net";
	p->titlesize = 24;
	p->subtitlesize = 20;
	p->spacebetweenlines = 10;
	p->spacebetweenparagraphs = 40;
	p->margin = 60;
	return p;
}
#endif
