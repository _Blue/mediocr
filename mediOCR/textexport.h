#ifndef TEXTEXPORT_H
#define TEXTEXPORT_H



bool export_txt(_in_ const char *text,_in_ const char *path);
/*Export specified letter* array in specified path*/

#ifndef NO_PDF

typedef struct
{
    const char *font;
    const char *title;
    const char *subtitle;
    USHORT size;
    USHORT linesize;
    USHORT titlesize;
    USHORT subtitlesize;
    USHORT spacebetweenparagraphs;
    USHORT spacebetweenlines;
    USHORT margin;

} PdfExportParams;

bool export_pdf(_in_ const char *data,_in_ const char *path,PdfExportParams *params);
/*Export specified letter* array in specified path in pdf format*/

PdfExportParams *get_defaultpdfparams();
#endif
#endif // TEXTEXPORT_H
