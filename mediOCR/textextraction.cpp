#include "stdafx.h"
#include "letter.h"
#include "textextraction.h"
///internal fonctions

static USHORT nextline(_inout_ Queue*, _in_ Bin_Image *img, USHORT line,
	Rect r);
static bool is_subcolumnblank(_in_ Bin_Image *img, USHORT x, USHORT y,
	USHORT h);
static bool is_sublineblank(_in_ Bin_Image *img, USHORT x, USHORT u, USHORT w);

static void adjust_letterrect(_inout_ Bin_Image *img, _inout_ Letter *,
	_in_ Rect *line);
static void adjust_linerect(_in_ Bin_Image *img, _inout_ Rect*);
static void adjust_blockrect(_inout_ Rect *r, USHORT w, USHORT h);
static void adjust_letterrect_sub(_inout_ Bin_Image *img, _inout_ Letter *,
	USHORT x, USHORT y, _in_ Rect*, _out_ Queue *points);

static Letter **finalizedetection(_in_ Image *img, _in_ Bin_Image* bimg,
	USHORT woffset, USHORT hoffset,
	_in_ Queue *q, _out_ USHORT *count,
	bool colors);
static void finalizelines(USHORT woffset, USHORT hoffset, _inout_ Rect* lines,
	USHORT lcount);

static Pixel getrectcolor(_in_ Image *img, _in_ Bin_Image *binmg, Rect r,
	USHORT woffset, USHORT hoffset);

static Queue *extract_blocs(_in_ Bin_Image *img, USHORT rx, USHORT ry);
static bool nextblock(_inout_ Bin_Image *img, _inout_ Queue *queue);

static void get_rlsavalues(_in_ Bin_Image *img, _out_ USHORT *w,
	_out_ USHORT *h);
static void rlsa_computeline(_in_ Bin_Image *img, Rect rect, _out_ DWORD *tx,
	_out_ DWORD *cx);
static void rlsa_computeblock(_in_ Bin_Image *img, Rect rect, _out_ DWORD *ty,
	_out_ DWORD *cy);
static DWORD WINAPI rlsa_worker(void* params);

static bool merge_letterwithaccents(_inout_ QueueNode *queue,
	_in_ Letter *letter);

static void add_spaces(_inout_ QueueNode *queue, Rect line, Rect block);

static DWORD WINAPI recognizeletter_worker(void * data);

static void recognizeletters(_inout_ Letter **letters, USHORT count,
	_in_ Image *img);

static void space_posttreatment(_inout_ Queue *letters, _in_ Image *img);

static void paragraph_posttreatment(_inout_ Queue *letters);

///Internal structures
typedef struct
{
	Bin_Image *inimg;
	Bin_Image *outimg;
	unsigned short start;
	unsigned short rlsacx;
	unsigned short rlsacy;
} rlsaworkerparams;

typedef struct
{
	Letter **letters;
	Image *img;
	USHORT number;
	USHORT start;
} neuralworkerparams;

///Internal variables
static USHORT irlsaradius = RLSA_C;
static USHORT irlsaforcedradius = 0;

#ifdef DEBUG_SAVE_LETTERS
static int tmp_foo_bar_toremove_i = 0;
#endif

OCRAPI void extract_lines(Bin_Image *img, Rect rect, Queue * queue)
{
	USHORT line = rect.y;
	while (line < rect.y + rect.h)
	{
		line = nextline(queue, img, line, rect);
	}
}

static USHORT nextline(Queue *q, Bin_Image *img, USHORT line, Rect rect)
{
	line--;
	do
	{
		line++;
		if (line == rect.y + rect.h)
			return line;
	} while (is_sublineblank(img, rect.x, line, rect.w));
	USHORT start = line;
	line++;
	USHORT x;
	while (line < rect.y + rect.h)
	{
		for (x = rect.x; x < rect.x + rect.w; x++)
		{
			if (getpixelbin(img, x, line))
			{
				Rect r = getrectcontaining_shape(img, x, line);
				if (r.y < line - 1)
					break;
			}
		}
		if (x == rect.x + rect.w)
			break;
		line++;
	}
	Line *l = (Line*)malloc(sizeof(Line));
	l->rect.h = line - start - 1;
	l->rect.w = rect.w;
	l->rect.x = rect.x;
	l->rect.y = start;
	l->block = rect;
	adjust_linerect(img, &l->rect);
	push(q, l);
	return line;
}


OCRAPI bool is_realletter(Letter *l)
{
	return l->value != ' ' && l->value != '\n';
}

void extract_chars(Bin_Image *img, Queue *lines, Queue *letters, Rect *rects)
{
	Line *line;
	Letter *l;
	USHORT i = 0;
	QueueNode *firstnode;
	while (!isqueueempty(lines))
	{
		line = (Line*)pop(lines);
		firstnode = NULL;
		USHORT x, y;
		for (x = line->rect.x; x < line->rect.x + line->rect.w; x++)
		{
			for (y = line->rect.y; y < line->rect.y + line->rect.h; y++)
			{
				if (getpixelbin(img, x, y))
				{
					l = (Letter*)malloc(sizeof(Letter));
					l->rect.x = x;
					l->rect.y = y;
					l->rect.w = 1;
					l->rect.h = 1;
					l->value = '\0';
					l->block = line->block;
					ZeroMemory(l->neuralresult, sizeof(char) * 255);
					adjust_letterrect(img, l, &line->rect);
					if (firstnode != NULL && merge_letterwithaccents(firstnode, l))
					{
						free_bimage(l->data);
						free(l);
					}
					else
					{
						push(letters, l);
						if (firstnode == NULL)
							firstnode = letters->last;
					}
				}
			}
		}
		if (rects != NULL)
		{
			rects[i] = line->rect;
			i++;
		}
		if (firstnode != NULL)
			add_spaces(firstnode, line->rect, line->block);
		free(line);
	}
	freequeue(lines);
}

static OCRAPI bool merge_letterwithaccents(QueueNode *node, Letter * l)
{
	while (node != NULL) //Using queue like a linked list
	{
		if (l->rect.x >= ((Letter*)node->data)->rect.x && l->rect.x <= ((Letter*)node->data)->rect.x + ((Letter*)node->data)->rect.w)
		{
			Letter *upper = l;
			Letter *lower = (Letter*)node->data;
			if ((lower->rect.x + lower->rect.w - l->rect.x) / (float)l->rect.w >=
				MIN_ACCENT_COMON_RATION)
			{
				if (upper->rect.y > lower->rect.y)
				{
					Letter *b = upper;
					upper = lower;
					lower = b;
				}
				if (upper->rect.y + upper->rect.h <= lower->rect.y + lower->rect.h)
					addletter((Letter*)node->data, *l);
				return true;
			}
		}
		node = node->next;
	}
	return false;
}

static OCRAPI void add_spaces(QueueNode *q, Rect line, Rect block)
{
	typedef struct
	{
		int distance;
		QueueNode *node;
	} lvalue;
	QueueNode *node = q;
	USHORT count = 0;
	USHORT i = 0;
	while (node != NULL)
	{
		count++;
		node = node->next;
	}
	if (count < 2)
		return;
	lvalue *values = (lvalue*)malloc(sizeof(lvalue) * count);
	node = q;
	while (node != NULL)
	{
		values[i].node = node;
		i++;
		node = node->next;
	}
	for (i = 0; i < count - 1; i++)
	{
		values[i].distance = ((Letter*)values[i + 1].node->data)->rect.x - ((
			Letter*)values[i].node->data)->rect.x - ((Letter*)values[i].node->data)->rect.w;
	}
	long mid = 0;
	USHORT midcount = 0;
	for (i = 0; i < count - 1; i++)
	{
		if (values[i].distance > 1)
		{
			mid += values[i].distance;
			midcount++;
		}
	}
	mid = (long)round(mid / (float)midcount);
	long adjustedmid = 0;
	midcount = 0;
	for (i = 0; i < count - 1; i++)
	{
		if (values[i].distance > mid)
		{
			adjustedmid += values[i].distance;
			midcount++;
		}
	}
	adjustedmid = (long)(round(adjustedmid / (float)midcount));
	for (i = 0; i < count - 1; i++)
	{
		if ((values[i].distance > 1) && (values[i].distance > adjustedmid
			|| values[i].distance / (float)adjustedmid > WORD_SPACE_MIN_RATIO))
		{
			Letter *l = (Letter*)malloc(sizeof(Letter));
			l->color = DEFAULT_SPACECOLOR;
			l->rect.x = ((Letter*)values[i].node->data)->rect.x + ((Letter*)values[i].node->data)->rect.w;
			l->rect.w = values[i].distance;
			l->rect.y = line.y;
			l->rect.h = line.h;
			l->block = block;
			l->value = ' ';
#ifdef BUILD_DEBUG
			l->data = NULL;
			l->originaldata = NULL;
#endif
			queue_insertafter(values[i].node, l);
		}
	}
	free(values);
}

static OCRAPI bool is_subcolumnblank(Bin_Image *img, USHORT x, USHORT y,
	USHORT h)
{
	while (h > 0 && !getpixelbin(img, x, y))
	{
		y++;
		h--;
	}
	return h == 0;
}

static OCRAPI bool is_sublineblank(Bin_Image *img, USHORT x, USHORT y, USHORT w)
{
	while (w > 0 && !getpixelbin(img, x, y))
	{
		x++;
		w--;
	}
	return w == 0;
}

static OCRAPI void adjust_letterrect(Bin_Image *img, Letter *l, Rect *line)
{
	Queue *points = createqueue();
	adjust_letterrect_sub(img, l, l->rect.x, l->rect.y, line, points);
	if (getpixelbin(img, line->x, line->y))
		push(points, createpoint(line->x, line->y));

	l->data = create_bin_image(l->rect.w, l->rect.h);
	ZeroMemory(l->data->data, sizeof(bool)*l->data->w * l->data->h);
	Point *p;
	while (!isqueueempty(points))
	{
		p = (Point*)pop(points);
		setpixelbin(l->data, p->x - l->rect.x, p->y - l->rect.y, true);
		free(p);
	}
	freequeue(points);
}

static void adjust_letterrect_sub(Bin_Image *img, Letter *l, USHORT x, USHORT y,
	Rect *line, Queue *points)
{
#ifdef BUILD_DEBUG
	if (!getpixelbin(img, x, y))
	{
		critical("adjust_letterrect_sub called with a non-black point");
		return;
	}
#endif
	setpixelbin(img, x, y, false);
	Queue *q = createqueue();
	push(q, createpoint(x, y));
	Rect *r = &l->rect;
	Point *p;
	while (!isqueueempty(q))
	{
		p = (Point*)pop(q);
		x = p->x;
		y = p->y;
		push(points, p);
		if (x < r->x)
		{
			r->x--;
			r->w++;
		}
		else if (x >= r->x + r->w)
		{
			r->w++;
		}
		if (y < r->y)
		{
			r->y--;
			r->h++;
		}
		else if (y >= r->y + r->h)
		{
			r->h++;
		}
		if (x > line->x && getpixelbin(img, x - 1, y)) //left
		{
			push(q, createpoint(x - 1, y));
			setpixelbin(img, x - 1, y, false);
		}
		if (x < line->w + line->x - 1 && getpixelbin(img, x + 1, y)) //right
		{
			push(q, createpoint(x + 1, y));
			setpixelbin(img, x + 1, y, false);
		}
		if (y > line->y && getpixelbin(img, x, y - 1)) //up
		{
			push(q, createpoint(x, y - 1));
			setpixelbin(img, x, y - 1, false);
		}
		if (y < line->y + line->h - 1 && getpixelbin(img, x, y + 1)) //down
		{
			push(q, createpoint(x, y + 1));
			setpixelbin(img, x, y + 1, false);
		}
		if (x > line->x && y > line->y && getpixelbin(img, x - 1, y - 1)) //left up
		{
			push(q, createpoint(x - 1, y - 1));
			setpixelbin(img, x - 1, y - 1, false);
		}
		if (x < line->x + line->w - 1 && y < line->y + line->h - 1
			&& getpixelbin(img, x + 1, y + 1)) //Right down
		{
			push(q, createpoint(x + 1, y + 1));
			setpixelbin(img, x + 1, y + 1, false);
		}
		if (x > line->x && y < line->y + line->h - 1
			&& getpixelbin(img, x - 1, y + 1)) //Left down
		{
			push(q, createpoint(x - 1, y + 1));
			setpixelbin(img, x - 1, y + 1, false);
		}
		if (y > line->y && x < line->x + line->w - 1
			&& getpixelbin(img, x + 1, y - 1)) //right up
		{
			push(q, createpoint(x + 1, y - 1));
			setpixelbin(img, x + 1, y - 1, false);
		}
	}
	freequeue(q);
}

OCRAPI void adjust_linerect(Bin_Image *img, Rect *r)
{
	while (r->w > 0 && is_subcolumnblank(img, r->x, r->y, r->h))
	{
		r->x++;
		r->w--;
	}
	while (r->w > 0
		&& is_subcolumnblank(img, MIN(r->x + r->w, img->w - 1), r->y, r->h))
	{
		r->w--;
	}
}

static Pixel getrectcolor(Image *img, Bin_Image *bimg, Rect rect,
	USHORT woffset, USHORT hoffset)
{
	USHORT x;
	USHORT y;
	DWORD r = 0;
	DWORD g = 0;
	DWORD b = 0;
	DWORD count = 0;
	Pixel p;
	for (x = rect.x; x < rect.x + rect.w; x++)
	{
		for (y = rect.y; y < rect.y + rect.h; y++)
		{
			if (getpixelbin(bimg, x - woffset, y - hoffset))
			{
				p = getpixel(img, x, y);
				r += p.red;
				g += p.green;
				b += p.blue;
				count++;
			}
		}
	}
	p.red = toint(r / (float)count);
	p.green = toint(g / (float)count);
	p.blue = toint(b / (float)count);
	return p;
}

static Letter **finalizedetection(Image *img, Bin_Image *bimg, USHORT woffset, USHORT hoffset, Queue *q, USHORT *count, bool colors)
{
	*count = 0;
	Letter **ret = (Letter**)malloc(sizeof(Letter*) * q->count);
	Letter *l;
	while (!isqueueempty(q))
	{
		l = (Letter*)pop(q);
		l->rect.x += woffset;
		l->rect.y += hoffset;
		ret[*count] = l;
		if (is_realletter(ret[*count]))
		{
			if (colors)
				ret[*count]->color = getrectcolor(img, bimg,
				ret[*count]->rect, woffset, hoffset);
			else
				ret[*count]->color = DEFAULT_LETTERCOLOR;
		}
		(*count)++;
	}
	freequeue(q);
	return ret;
}

static OCRAPI void adjust_blockrect(Rect *r, USHORT w, USHORT h)
{
	r->x += w;
	r->y += h;
}


static Queue *extract_blocs(Bin_Image *img, USHORT rx, USHORT ry)
{
	Bin_Image *buf = create_bin_image(img->w, img->h);
	memcpy(buf->data, img->data, sizeof(bool)*img->w * img->h);
	USHORT i;
	HANDLE *threads = (HANDLE *)malloc(sizeof(HANDLE) * get_maxthreads());
	rlsaworkerparams *params = (rlsaworkerparams*)malloc(sizeof(rlsaworkerparams) * get_maxthreads());
	for (i = 0; i < get_maxthreads(); i++)
	{
		params[i].inimg = img;
		params[i].outimg = buf;
		params[i].rlsacx = rx;
		params[i].rlsacy = ry;
		params[i].start = i;
		threads[i] = CreateThread(NULL, NULL, rlsa_worker, &params[i], NULL, NULL);
		if (threads[i] == NULL)
		{
			critical("CreateThread failed, %i", GetLastError());
			return NULL;
		}
	}

	for (i = 0; i < get_maxthreads(); i++)
	{
		WaitForSingleObject(threads[i], INFINITE);
		CloseHandle(threads[i]);
	}

	free(params);
	free(threads);

	Queue *q = createqueue();
	while (true)
	{
		if (!nextblock(buf, q))
			break;
	}
	free_bimage(buf);
	return q;
}

static DWORD WINAPI rlsa_worker(void *params)
{
	rlsaworkerparams *opt = (rlsaworkerparams*)params;
	USHORT x;
	USHORT y;
	USHORT cx;
	USHORT cy;
	int lastx;
	Bin_Image *buf = opt->outimg;
	Bin_Image *img = opt->inimg;
	for (y = opt->start; y < img->h; y += get_maxthreads())
	{
		lastx = -1;
		for (x = 0; x < img->w; x++)
		{
			if (!getpixelbin(img, x, y))
			{
				if (lastx != -1 && ABS((float)(x - lastx)) <= opt->rlsacx)
				{
					setpixelbin(buf, x, y, true);
				}
				else
				{
					lastx = -1;
					for (cx = MIN(x + opt->rlsacx, img->w - 1) + 1; cx > x - opt->rlsacx - 1
						&& lastx == -1 && cx > 0; cx--)
					{
						for (cy = MAX(y - opt->rlsacy, 0); cy < y + opt->rlsacy && cy < img->h; cy++)
						{
							if (getpixelbin(img, cx - 1, cy))
							{
								setpixelbin(buf, x, y, true);
								lastx = cx - 1;
								break;
							}
						}
					}
				}
			}
		}
	}
	return 0;
}

static bool nextblock(Bin_Image *img, Queue *q)
{
	USHORT x;
	USHORT y;
	for (y = 0; y < img->h; y++)
	{
		for (x = 0; x < img->w; x++)
		{
			if (getpixelbin(img, x, y))
			{
				Rect *r = (Rect*)malloc(sizeof(Rect));
				*r = getrectcontaining_shape(img, x, y);
				push(q, r);
				for (x = r->x; x < r->x + r->w; x++)
				{
					for (y = r->y; y < r->y + r->h; y++)
					{
						setpixelbin(img, x, y, false);
					}
				}
				return true;
			}
		}
	}
	return false;
}

static OCRAPI void finalizelines(USHORT w, USHORT h, Rect *lines,
	USHORT lc)//Usefull only if drawing is needed
{
	USHORT i;
	for (i = 0; i < lc; i++)
	{
		lines[i].x += w;
		lines[i].y += h;
	}
}

OCRAPI static void rlsa_computeline(Bin_Image *img, Rect r, DWORD *total,
	DWORD *count)
{
	USHORT x = r.x;
	USHORT start;
	while (x < r.x + r.h)
	{
		while (x < r.w + r.x && !is_subcolumnblank(img, x, r.y, r.h))
		{
			x++;
		}
		if (x == r.w + r.x)
			break;
		start = x;
		while (x < r.w + r.x)
		{
			if (!is_subcolumnblank(img, x, r.y, r.h))
			{
				*total += x - start;
				(*count)++;
				break;
			}
			x++;
		}
	}
}

OCRAPI static void rlsa_computeblock(Bin_Image *img, Rect r, DWORD *total,
	DWORD *count)
{
	USHORT y = r.y;
	USHORT start;
	while (y < r.y + r.h)
	{
		while (y < r.h + r.y && !is_sublineblank(img, r.x, y, r.w))
		{
			y++;
		}
		if (y == r.y + r.h)
			break;
		start = y;
		while (y < r.h + r.y)
		{
			if (!is_sublineblank(img, r.x, y, r.w))
			{
				*total += y - start;
				(*count)++;
				break;
			}
			y++;
		}
	}
}

static void get_rlsavalues(Bin_Image *img, USHORT *w, USHORT *h)
{
	DWORD totalx = 0;
	DWORD countx = 0;
	DWORD totaly = 0;
	DWORD county = 0;
	Queue *blocks = extract_blocs(img, irlsaradius, irlsaradius);
	Queue *lines = createqueue();
	Rect *rect;
	while (!isqueueempty(blocks))
	{
		rect = (Rect*)pop(blocks);
		rlsa_computeblock(img, *rect, &totaly, &county);
		extract_lines(img, *rect, lines);
		free(rect);
	}
	freequeue(blocks);
	while (!isqueueempty(lines))
	{
		rect = (Rect*)pop(lines);
		rlsa_computeline(img, *rect, &totalx, &countx);
		free(rect);
	}
	freequeue(lines);
	*w = MAX((int)(toint(totalx / (float)countx) * 4.5), 5);
	*h = toint(totaly / (float)county) + 3;
}

OCRAPI void setrlsa_radius(USHORT r)
{
	irlsaradius = r;
}

OCRAPI void forcerlsa_radius(USHORT r)
{
	irlsaforcedradius = r;
}

static OCRAPI void recognizeletters(Letter **letters, USHORT number, Image *img)
{
	neuralworkerparams *params = (neuralworkerparams*)malloc(sizeof(neuralworkerparams) *get_maxthreads());
	HANDLE *threads = (HANDLE*)malloc(sizeof(HANDLE) * get_maxthreads());
	USHORT i;
	for (i = 0; i < DEFAULT_NN_MAXTHREADS; i++)
	{
		params[i].start = i;
		params[i].number = number;
		params[i].letters = letters;
		params[i].img = img;
		threads[i] = CreateThread(NULL, NULL, recognizeletter_worker, &params[i], NULL, NULL);
		if (threads[i] == NULL)
		{
			critical("CreateThread failed, %i", GetLastError());
			return;
		}
	}

	for (i = 0; i < DEFAULT_NN_MAXTHREADS; i++)
	{
		WaitForSingleObject(threads[i], INFINITE);
	}
	free(threads);
	free(params);
}

static DWORD WINAPI recognizeletter_worker(void *data)
{
	neuralworkerparams *opt = (neuralworkerparams*)data;
	USHORT i;
	for (i = opt->start; i < opt->number; i += DEFAULT_NN_MAXTHREADS)
	{
		if (!is_realletter(opt->letters[i]))
			continue;
#ifdef DEBUG_SAVE_LETTERS
		char buf[512];
		sprintf(buf, "tmp/%i.bmp", tmp_foo_bar_toremove_i);
		Image *img = bimage_tosurface(opt->letters[i]->data);
		save_image(img, buf);
		free_image(img);
#endif
		USHORT w, h;
		get_size(&w, &h);
		prepareletter_fornn(opt->letters[i], w, h, opt->img);
		get_sletter(opt->letters[i]);
#ifdef DEBUG_SAVE_LETTERS
		sprintf(buf, "tmp/%i.resized.bmp", tmp_foo_bar_toremove_i);
		img = bimage_tosurface(opt->letters[i]->data);
		save_image(img, buf);
		free_image(img);
		tmp_foo_bar_toremove_i++;
#endif
	}
	return NULL;
}

static OCRAPI void space_posttreatment(Queue *letters, Image *img)
{
	if (letters->first == NULL)
	{
		return;
	}
	QueueNode *node = letters->first;
	QueueNode *startnode = node;
	Rect currentrect = ((Letter*)node->data)->block;
	Letter *l;
	while (node != NULL)
	{
		unsigned long mid = 0;
		unsigned long count = 0;
		l = (Letter*)node->data;
		if (l->value == ' ')
		{
			mid += l->rect.w;
			count++;
			if (!rect_equals(l->block, currentrect))
			{
				mid = (ULONG)round(mid / (float)count);
				l = (Letter*)startnode->data;
				while (startnode != NULL && rect_equals(l->block, currentrect))
				{
					if (l->value == ' ')
					{
						if (l->rect.w / (float)mid < 0.5)
						{
							free(l);
							startnode = startnode->previous;
							queue_remove_node(startnode->next);
						}
					}
					startnode = startnode->next;
					l = (Letter*)startnode->data;
				}
				mid = 0;
				count = 0;
				currentrect = l->block;
			}
		}
		node = node->next;
	}
	node = letters->first;
	USHORT x = 0;
	currentrect = ((Letter*)node->data)->block;
	while (node != NULL)
	{
		l = (Letter*)node->data;
		if (l->rect.x < x && node->next != NULL
			&& rect_equals(((Letter*)node->next->data)->block, currentrect))
		{
			l = (Letter*)node->previous->data;
			Letter *space = (Letter*)malloc(sizeof(Letter));
			space->rect.x = l->rect.x + l->rect.w;
			space->rect.y = l->rect.y;
			space->rect.h = l->rect.h;
			if (l->rect.w + l->rect.x + 1 < img->w)
				space->rect.w = 1;
			space->value = ' ';
			space->color = DEFAULT_SPACECOLOR;
#ifdef BUILD_DEBUG
			space->data = NULL;
			space->originaldata = NULL;
#endif
			space->block = currentrect;
			queue_insertbefore(node, space);
			l = (Letter*)node->data;
		}
		currentrect = l->block;
		node = node->next;
		x = l->rect.x;
	}
}

static void paragraph_posttreatment(Queue *letters)
{
	QueueNode *node = letters->first;
	if (node == NULL)
		return;
	Rect currentrect = ((Letter*)letters->first->data)->block;
	while (node != NULL)
	{
		if (!rect_equals(currentrect, ((Letter*)node->data)->block) && node->next != NULL)
		{
			Letter *l = (Letter*)malloc(sizeof(Letter));
			l->value = '\n';
			l->color = DEFAULT_INTERBLOCKCOLOR;
			l->block = currentrect;
#ifdef BUILD_DEBUG
			l->data = NULL;
			l->originaldata = NULL;
#endif
			l->rect.x = ((Letter*)node->data)->rect.x + ((Letter*)node->data)->rect.w;
			l->rect.w = 1;
			l->rect.y = ((Letter*)node->data)->rect.y;
			l->rect.h = ((Letter*)node->data)->rect.h;
			queue_insertbefore(node, l);
			currentrect = ((Letter*)node->data)->block;
		}
		node = node->next;
	}
}
Letter **detect_letters_internal(Image *img, bool colors, int btype,
	int threshold, bool noise, bool rotation,
	USHORT *lettercount, Rect **lines_out, USHORT *linecount_out, Rect **blocs_out,
	USHORT *blockcount_out)
{
	///Init
	LARGE_INTEGER t = init_time();
	USHORT linecount;
	Rect *rect;
	USHORT rslax;
	USHORT rslay;
	USHORT w = img->w;
	USHORT h = img->h;
	start_lnn();

	///Binarization
	Bin_Image *bin = binarize(img, btype, threshold, noise, rotation);
	verbose("Binarized (%lu ms)\r\n", timediff(t));

	remove_blank(bin, &w, &h);
	verbose("Removed blank (%lu ms)\r\n", timediff(t));

	///Block detection
	if (irlsaforcedradius)
	{
		rslax = irlsaforcedradius;
		rslay = irlsaforcedradius;
		printf("Using forced rlsa value : %u\r\n", rslax);
	}
	else
	{
		get_rlsavalues(bin, &rslax, &rslay);
		verbose("Computed rlsa values (used up to %u threads) (%u, %u) (%lu ms)\r\n", get_maxthreads(), rslax, rslay, timediff(t));
	}

	Queue *blocks = extract_blocs(bin, rslax, rslay);
	verbose("Extracted text blocs (used up to %u threads) (%lu ms)\r\n", get_maxthreads(), timediff(t));

	///Line detection
	Queue *lines = createqueue();
	if (blocs_out != NULL && blockcount_out != NULL)
	{
		*blocs_out = (Rect*)malloc(sizeof(Rect)*blocks->count);
		*blockcount_out = 0;
	}
	while (!isqueueempty(blocks))
	{
		rect = (Rect*)pop(blocks);
		extract_lines(bin, *rect, lines);
		adjust_blockrect(rect, w, h);
		if (blocs_out != NULL && blockcount_out != NULL)
		{
			(*blocs_out)[*blockcount_out] = *rect;
			(*blockcount_out)++;
		}
		free(rect);
	}
	freequeue(blocks);
	verbose("Extracted lines (%lu ms)\r\n", timediff(t));

	///Letter detection
	Queue *chars = createqueue();
	linecount = lines->count;
	Rect *linerect = (Rect*)malloc(sizeof(Rect) * linecount);
	extract_chars(bin, lines, chars, linerect);
	verbose("Extracted characters (%lu ms)\r\n", timediff(t));
	space_posttreatment(chars, img);
	paragraph_posttreatment(chars);

	Letter **letters = finalizedetection(img, bin, w, h, chars, lettercount, colors);
	///Letter recognition

	recognizeletters(letters, *lettercount, img);
	verbose("Detected characters (used up to %u threads) (%lu ms)\r\n", get_maxthreads(), timediff(t));

	if (lines_out != NULL && linecount_out != NULL) //Outgoing data
	{
		finalizelines(w, h, linerect, linecount);
		*lines_out = linerect;
		*linecount_out = linecount;
	}
	else
	{
		free(linerect);
	}
	free_bimage(bin);
	quit_lnn();
	verbose("Cleaned up (%lu ms)\r\n", timediff(t));
	return letters;
}

Letter **detect_anddraw(_inout_ Image *img, bool colors, int btype, int threshold, bool noise, bool rotation, USHORT *lettercount)
{
	Rect *blocks;
	Rect *lines;
	USHORT blockcount;
	USHORT linecount;
	Letter **letters = detect_letters_internal(img, colors, btype, threshold, noise, rotation, lettercount, &lines, &linecount, &blocks, &blockcount);
	int i;
	for (i = 0; i < blockcount; i++)
	{
		drawfromrect(img, blocks[i], DEFAULT_BLOCKCOLOR);
	}
	free(blocks);

	for (i = 0; i < *lettercount; i++)
	{
		drawfromrect(img, letters[i]->rect, letters[i]->color);
	}
#ifdef DRAW_LINES
	for (i = 0; i < linecount; i++)
	{
		drawfromrect(img, lines[i], DEFAULT_LINECOLOR);
	}
#endif
	free(lines);
	return letters;
}
