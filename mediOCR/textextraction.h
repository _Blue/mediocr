/// File : textextraction.h
///Purpose : Contains fonction that detects lines, blocs and letters

#ifndef TEXTEXTRACTION_H
#define TEXTEXTRACTION_H
#include "stdafx.h"
#include "imgmanip.h"
#include "queue.h"
#include "filters.h"

void extract_lines(_in_ Bin_Image*, Rect rect, _inout_ Queue *buff);
/*Detects lines from a Bin_Image, safe is image and rect are valid
Detected rectangles are stored in buff*/

void extract_chars(_in_ Bin_Image *img, _inout_ Queue *lines,
                   _inout_ Queue *letters, _inoutopt_ Rect *rects);
/*Detects chars from lines in a Bin_Image, safe is the image is valid,
parameter rects will contains lines rect if non zero
Detected rectangles are stored in buff*/

Letter **detect_anddraw(_inout_ Image *img, bool colors, int btype, int threshold,bool noise,bool rotation,USHORT *count);
/*Detects letters and marks them in rectangles, if volotd is true, colors
will also be detected*/

void setrlsa_radius(USHORT radius);
/* overrides default rlsa radius*/

void forcerlsa_radius(USHORT radius);
/* overrides rlsa radius calculation*/

bool is_realletter(_in_ Letter *l);
//returns true if l is a real letter (not a space or a \n

Letter **detect_letters_internal(_inout_ Image *img, bool colors, int btype,
                                 int threshold, bool noise, bool rotation, _out_ USHORT *count,
                                 _outopt_ Rect **lines, _outopt_ USHORT *linecount, _outopt_ Rect **blocks,
                                 _outopt_ USHORT *blockcount);
/*Returns detected letters in specified image*/
#define detect_letters(img,colors,type,t,noise,rotation,count) detect_letters_internal((img),(colors),(type),(t),(noise),(rotation),(count),NULL,NULL,NULL,NULL)
#endif
