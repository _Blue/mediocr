#include "stdafx.h"
#include "xor.h"

static void xor_learning(int nb)
{
    double values[4][3] =
    {{0, 0,   0},
     {0, 1,   1},
     {1, 0,   1},
     {1, 1,   0}
    };

    double outputs;

    int size_layer[] = {2, 4, 1};
    struct network * n = new_network(3, size_layer);

    int i,k;
    for (i=0; i <= nb; i++)
    {
        k = (int)(4.0*rand()/(RAND_MAX+1.0));
        network_treatment(n,&values[k][0]);
        back_prop(n,&values[k][2]);

        printf("=====%d/%d====\n", i, nb);

        network_treatment(n,&values[0][0]); get_outputs(n,&outputs);
        printf("0 xor 0 = \t %f\n", outputs);

        network_treatment(n,&values[1][0]); get_outputs(n,&outputs);
        printf("0 xor 1 = \t %f\n", outputs);

        network_treatment(n,&values[2][0]); get_outputs(n,&outputs);
        printf("1 xor 0 = \t %f\n", outputs);

        network_treatment(n,&values[3][0]); get_outputs(n,&outputs);
        printf("1 xor 1 = \t %f\n", outputs);

        printf("\n");
    }   

    free_network(n);
}   

void xor(int nb)
{
    xor_learning(nb);
}

void xor_default()
{
    xor_learning(3500);
}
