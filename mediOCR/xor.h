#ifndef XOR_H
#define XOR_H

#include <stdio.h>
#include <stdlib.h>

#include "neurone.h"

void xor(int nb);
/*Learning xor function             */
/*Param 1: number or learning to do */

void xor_default();
/*Learning xor function             */

#endif
