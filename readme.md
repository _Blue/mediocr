##mediOCR for Windows
A fork of the [mediOCR project](http://mediocr.net)

##Build
To build mediOCR for Windows, you need Microsoft Visual Studio 2013.4.  
The pdf exportation is based on [libHaru](http://libharu.org)
This project also uses [zlib](http://zlib.net).  
The image decoding is based on [Gdiplus](https://msdn.microsoft.com/en-us/library/windows/desktop/ms533798(v=vs.85).aspx).
For your convenience, all needed files have to been added to the repository so you do not have to install any of the previously written libraries.  
###Buid targets
mediOCR comes with tree build targets:  

* **Debug**, used for development 
* **Release**, Normal build
* **Release - no pdf"**, Build without pdf support

##Authors
Fork by Blue <admin@bluecode.fr>

##Features
* Supported image formats: bmp, png, jpg, gif.
* Text exportation (txt or pdf)
* Supports rotated images
* Supports noisy images
* Supports dictionaries to correct the letter recognition eventual mistakes

##Links
[mediOCR's website](http://mediocr.net)  
[Blue's blog](http://bluecode.fr)